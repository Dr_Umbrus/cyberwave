﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Concrete : Enemies
{
    public Sprite armSprite, pressSprite, shovelSprite;
    public GameObject shovel, press, arm;
    string whichAttack;


    public override void Pattern()
    {
        if (!stopped)
        {


            if (timeMove > 0)
            {
                timeMove -= Time.deltaTime;
            }
            else
            {
                timeMove = startTimeMove + Random.Range(-5, 6) / 10;
                timeAttack--;

                int x = 0;
                int y = 0;

                canMove = false;
                while (!canMove)
                {
                    x = Random.Range(gm.maxSens0, 8);
                    y = Random.Range(0, 4);

                    canMove = gm.CheckMove(x, y, 1, 0, 2);
                    if(x==posX && y == posY)
                    {
                        canMove = false;
                    }
                }

                posX = x;
                posY = y;
                transform.position = gm.spawnPosR[4 * (x - 4) + y] + new Vector3(0, offset, -5);
            }

            if (timeAttack <= 0)
            {
                timeAttack = Random.Range(1, 4);
                int potato = Random.Range(0, 101);

                if (potato < 66)
                {
                    anim.SetTrigger("Shovel");
                    attacking = true;
                    stopped = true;

                    whichAttack = "Shovel";
                }
                else if (potato < 86)
                {
                    anim.SetTrigger("Arm");
                    attacking = true;
                    stopped = true;
                    whichAttack = "Arm";
                }
                else
                {
                    anim.SetTrigger("Press");
                    attacking = true;
                    stopped = true;
                    whichAttack = "Press";
                }
            }
        }

        if(whichAttack == "Shovel" && sr.sprite == shovelSprite && attacking)
        {
            int potato=Random.Range(0, 2);
            if (posY == 3)
            {
                potato = 1;
            }
            if (posY == 0)
            {
                potato = 0;
            }
            

            if (potato == 0)
            {
                Instantiate(shovel, new Vector3(transform.position.x, transform.position.y - offset+1.1f, transform.position.z+0.1f), Quaternion.identity);
            }
            else if(potato==1)
            {
                Instantiate(shovel, new Vector3(transform.position.x, transform.position.y - offset - 1.1f, transform.position.z-0.1f), Quaternion.identity);
            }
            else
            {
                Instantiate(shovel, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z - 0.1f), Quaternion.identity);
            }
            
            attacking = false;
        }

        if(whichAttack == "Arm" && sr.sprite == armSprite && attacking)
        {
            GameObject temp=Instantiate(arm, new Vector3(transform.position.x-4*2.15f, transform.position.y - offset - 0.55f, transform.position.z), Quaternion.identity);
            temp.GetComponent<ConcreteArm>().Stats(posX - 4, posY, gm, transform.position.y - offset);

            attacking = false;
        }

        if (whichAttack == "Press" && sr.sprite == pressSprite && attacking)
        {
            Instantiate(press, transform.position, Quaternion.identity);

            attacking = false;
        }

    }
}
