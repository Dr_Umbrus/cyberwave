﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrest : TileIdentity
{
    public float lifetime;
    public CRS father;
    Player player;
    SpriteRenderer targetTile;
    SpriteRenderer sr;
    public float blinkTime, maxBlinkTime;
    public Color startCol, clignoteCol;
    int counter = 3;
    Bro[] bros;

    TileIdentity grabbed = null;

    public override void Starting()
    {
        sr = GetComponent<SpriteRenderer>();
        sr.enabled = false;
        player = FindObjectOfType<Player>();
        posX = player.posX;
        posY = player.posY;
        for(int i=0; i<gm.battleSol.Count; i++)
        {
            if(posX==gm.battleSol[i].posX && posY == gm.battleSol[i].posY)
            {
                transform.position = gm.battleSol[i].transform.position;
                transform.Translate(new Vector3(0, 0, -3));
                targetTile = gm.battleSol[i].GetComponent<SpriteRenderer>();    
            }
        }

        startCol = targetTile.color;
        bros = FindObjectsOfType<Bro>();
    }

    public void Offset(int x, int y)
    {
        posX += x;
        posY += y;

        for (int i = 0; i < gm.battleSol.Count; i++)
        {
            if (posX == gm.battleSol[i].posX && posY == gm.battleSol[i].posY)
            {
                transform.position = gm.battleSol[i].transform.position;
                transform.Translate(new Vector3(0, 0, -3));
                targetTile = gm.battleSol[i].GetComponent<SpriteRenderer>();
            }
        }
    }

    
    void Update()
    {
        if (counter > 0)
        {
            if (blinkTime >= 0)
            {
                blinkTime -= Time.deltaTime;

            }
            else
            {
                blinkTime = maxBlinkTime;
                if (targetTile.color == clignoteCol)
                {
                    targetTile.color = startCol;
                    counter--;
                }
                else
                {
                    targetTile.color = clignoteCol;
                }


            }
        }
        else
        {
            sr.enabled = true;
            lifetime -= Time.deltaTime;
            if (lifetime <= 0)
            {
                Destroy(gameObject);
            }
        }
        

        if (player.posX==posX && player.posY == posY && counter<=0)
        {
            player.side = 1;
            grabbed = player;
        }

        foreach(Bro bro in bros)
        {
            if (bro.posX == posX && bro.posY == posY && counter <= 0)
            {
                grabbed = bro;
            }
        }
    }

    private void OnDestroy()
    {
        gm.battleTerrain.Remove(this);
        gm.battleEntities.Remove(this);
        if (grabbed != null)
        {
            grabbed.side = 0;
        }
    }
}
