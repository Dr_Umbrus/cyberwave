﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lob : MonoBehaviour
{
    public int cases;
    public float speed, curve;
    float y=0, x=0;
    float targetx;
    bool Awaken = false;
    public GameObject explosion;
    public int posX, posY;
    int damage = 0;
    float paraTime = 0;

    private void Awake()
    {
        
        x = transform.position.x;
        Time.timeScale = 1;
        y = transform.position.y;
        targetx = transform.position.x + (2.1f * cases / 2);
        Awaken = true;
    }

    public void MoreDakka(int dakka, float dur)
    {
        damage += dakka;
        paraTime += dur;

    }

    public void SetStats(int ix, int igrec)
    {
        posX = ix + cases;
        posY = igrec;
    }

    private void Update()
    {
        if(Awaken)
            if(cases>0)
        transform.position = new Vector3(transform.position.x+(speed * Time.deltaTime), ((-(Mathf.Pow(transform.position.x + (speed * Time.deltaTime) - targetx, 2))+Mathf.Pow(x-targetx,2))/curve)+y, transform.position.z);
            else
            {
                transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), ((-(Mathf.Pow(transform.position.x - (speed * Time.deltaTime) - targetx, 2)) + Mathf.Pow(x - targetx, 2)) / curve) + y, transform.position.z);
            }
        if (transform.position.y < y)
        {
            if((cases>0 && transform.position.x>targetx )|| (cases<0 && transform.position.x < targetx))
            {
                GameObject temp=Instantiate(explosion, transform.position, Quaternion.identity);
                temp.GetComponent<ExplosionLob>().SetStats(posX, posY);
                Destroy(gameObject);
            }
            
            
        }
    }
}
