﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualManager : MonoBehaviour
{
    //The visual manager, for the menu. Maybe also the UI ? I'm not sure.

    //Direct line to the Scroll manager.
    public ScrollManager sm;
    public GameManager gm;
    public Player player;
    public Cursored cursor;

    public Text named;
    public Image exemple;
    public Image[] hand;
    public Image[] letters;
    public Image element1, element2;
    public Text damage;
    public Text description;
    public bool[] selected;
    public Image[] selectedIcons;
    public int[] whichSelected;
    //Singleton
    private GameObject _instance;
    AudioSource audioS;
    int nextSelectedIcon = 0;
    public AudioClip valide, retour, ouverture;

    //public int price;

    [Header("Deck")]
    #region deck
    public float blinkSpeed;

    public GameObject editBase;
    public GameObject deckBase;
    public GameObject reverseBase;
    public GameObject cursorDeck;
    public Vector3 cursorBasePos;
    public Vector3[] cursorPositions;
    public Text[] deckList;
    public Image[] deckLetters;
    public Text numberDeck;
    public Text deckStun;
    public Image deckImage;
    public Image deckElem1;
    public Image deckElemn2;
    public Text deckName;
    public Text deckscription;
    public Text deckDamage;

    public int pages = 0;
    public int maxpage = 0;
    int reservePage = 0;
    int maxreservePage = 0;


    //0 et pas en menu, 1 = deck, 2=réserve
    public int deck = 0;
    public int cursorPos=0;
    bool vMove = true;

    public Text[] quantity;


    ScrollStats targetName;
    int targetPos;
    bool targetSide;
    int targetPage;
    int targetText;

    public Color textColor, transp;


    #endregion

    private void Awake()
    {
        if (audioS == null)
        {
            audioS = GetComponent<AudioSource>();
        }

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this.gameObject;
        }


    }

    public void ChangePage(int page)
    {
        if (deck == 1)
        {
            pages = page;
            for (int i = 0; i < 7; i++)
            {
                if (sm.deck.Count > i + 7 * page)
                {
                    deckList[i].text = sm.names[sm.deck[i + 7 * page].spell].ToString();
                    deckLetters[i].sprite = sm.letters[sm.deck[i + 7 * page].letter];
                    deckLetters[i].color = Color.white;
                    quantity[i].text = "";
                }
                else
                {
                    deckList[i].text = "";
                    deckLetters[i].color = Color.clear;
                    quantity[i].text = "";
                }
            }
        }
        else
        {
            reservePage = page;

            for (int i = 0; i < 7; i++)
            {
                if (sm.reserve.Count > i + 7 * page)
                {
                    deckList[i].text = sm.names[sm.reserve[i + 7 * page].spell].ToString();
                    deckLetters[i].sprite = sm.letters[sm.reserve[i + 7 * page].letter];
                    deckLetters[i].color = Color.white;
                    quantity[i].text = sm.reserve[i + 7 * page].ammount.ToString();
                }
                else
                {
                    deckList[i].text = "";
                    deckLetters[i].color = Color.clear;
                    quantity[i].text = "";
                }
            }
        }
    }

    public void ShowDescription()
    {
        if (deck == 1)
        {
            if(sm.deck.Count > cursorPos + 7 * pages)
            {
                deckName.text = sm.names[sm.deck[cursorPos + 7 * pages].spell];
                deckImage.sprite = sm.icones[sm.deck[cursorPos + 7 * pages].spell];
                deckElem1.sprite = sm.elements[sm.element1[sm.deck[cursorPos + 7 * pages].spell]];
                deckElemn2.sprite = sm.elements[sm.element2[sm.deck[cursorPos + 7 * pages].spell]];
                deckscription.text = sm.description[sm.deck[cursorPos + 7 * pages].spell];
                //Stun
                deckDamage.text = sm.damages[sm.deck[cursorPos + 7 * pages].spell];
                numberDeck.text = sm.deck.Count.ToString();
            }
            
        }
        else
        {
            if (sm.reserve.Count > cursorPos + 7 * pages)
            {
                deckName.text = sm.names[sm.reserve[cursorPos + 7 * pages].spell];
                deckImage.sprite = sm.icones[sm.reserve[cursorPos + 7 * pages].spell];
                deckElem1.sprite = sm.elements[sm.element1[sm.reserve[cursorPos + 7 * pages].spell]];
                deckElemn2.sprite = sm.elements[sm.element2[sm.reserve[cursorPos + 7 * pages].spell]];
                deckscription.text = sm.description[sm.reserve[cursorPos + 7 * pages].spell];
                //Stun
                deckDamage.text = sm.damages[sm.reserve[cursorPos + 7 * pages].spell];
            }
        }
                
        

    }

    public void ChangeSide(int side)
    {
        deck = side;
        cursorDeck.transform.localPosition = cursorBasePos;
        cursorPos = 0;
        if (deck == 1)
        {
            deckBase.SetActive(true);
            reverseBase.SetActive(false);
            ChangePage(0);

        }
        else
        {
            deckBase.SetActive(false);
            reverseBase.SetActive(true);
            reservePage = 0;
        }
        ChangePage(0);
    }

    public void SelectPower()
    {
        if (targetName == null)
        {
            if (deck == 1)
            {
                if (sm.deck.Count > cursorPos + 7 * pages)
                {
                    targetPos = cursorPos + 7 * pages;
                    targetName = sm.deck[targetPos];
                    targetSide = true;
                    targetPage = pages;
                    targetText = cursorPos;
                    ChangePage(pages);
                }
                
            }
            else
            {
                targetPos = cursorPos + 7 * reservePage;
                targetName = sm.reserve[targetPos];
                targetSide = false;
                targetPage = reservePage;
                targetText = cursorPos;
                ChangePage(reservePage);
            }
        }
        else
        {
            int newPos;
            ScrollStats newName;
            bool newSide;

            if (deck == 1)
            {
                newSide = true;
                newPos= cursorPos + 7 * pages;
                newName= sm.deck[newPos];

                if(newSide==targetSide && newName==targetName && newPos == targetPos)
                {
                   
                    ChangePowerSide(targetName, targetPos, targetSide);
                }
                else
                {
                    SwapPower(targetName, newName, targetPos, newPos, targetSide, newSide);
                }
                ChangePage(pages);
            }
            else
            {
                newSide = false;
                newPos = cursorPos + 7 * reservePage;
                newName = sm.reserve[newPos];

                if (newSide == targetSide && newName == targetName && newPos == targetPos)
                {
                    ChangePowerSide(targetName, targetPos, targetSide);
                }
                else
                {
                    SwapPower(targetName, newName, targetPos, newPos, targetSide, newSide);
                }
                ChangePage(reservePage);
            }
            
        }
        
    }

    public void ChangePowerSide(ScrollStats a, int posA, bool sideA)
    {
        if (sideA)
        {
            
            var potato = true;
            for (int i = 0; i < sm.reserve.Count; i++)
            {
                if (sm.reserve[i].spell == a.spell && sm.reserve[i].letter == a.letter)
                {
                    potato = false;
                    sm.reserve[i].ammount++;
                }
            }

            if (potato)
            {
                sm.reserve.Add(a);
            }

            sm.deck.RemoveAt(posA);
            ChangePage(pages);
        }
        else
        {
            if (sm.deck.Count < sm.maxDeck)
            {
                if (sm.reserve[posA].ammount >= 1)
                {
                    sm.deck.Add(a);
                    sm.reserve[posA].ammount--;
                }
                
            }
            else
            {
            }
        }

        targetName = null;
    }

    public void SwapPower(ScrollStats a, ScrollStats b, int posA, int posB, bool sideA, bool sideB)
    {
        //side = true = deck. Side = false = reserve

        if (sideA)
        {
            //deck to deck
            if (sideB)
            {
                Debug.Log(a.spell);
                Debug.Log(b.spell);
                sm.deck[posA] = b;
                sm.deck[posB] = a;
            }

            //deck to reserve
            else
            {
                var potato = true;
                for (int i = 0; i < sm.reserve.Count; i++)
                {
                    if(sm.reserve[i].spell == a.spell && sm.reserve[i].letter == a.letter)
                    {
                        potato = false;
                        sm.reserve[i].ammount++;
                    }
                }

                if (potato)
                {
                    sm.reserve.Add(a);
                }

                sm.reserve[posB].ammount--;
                sm.deck[posA] = b;
            }
        }

        else
        {
            //reserve to deck
            if (sideB)
            {
                var potato = true;
                for (int i = 0; i < sm.reserve.Count; i++)
                {
                    if (sm.reserve[i].spell == b.spell && sm.reserve[i].letter == b.letter)
                    {
                        potato = false;
                        sm.reserve[i].ammount++;
                    }
                }

                if (potato)
                {
                    sm.reserve.Add(b);
                }

                sm.reserve[posA].ammount--;
                sm.deck[posB] = a;
            }
            //reserve to reserve
            else
            {
                sm.reserve[posA] = b;
                sm.reserve[posB] = a;
            }
        }

        targetName = null;
    }



    private void Update()
    {
        if (deck != 0)
        {
            if ((Input.GetButtonDown("Down") || Input.GetAxis("Vax") < -0.3f) && vMove)
            {
                vMove = false;

                if (deck == 1)
                {
                    if ((cursorPos + 7 * pages == 27))
                    {
                        cursorPos = 0;
                        ChangePage(0);
                    }
                    else if (cursorPos < 6)
                    {
                        cursorPos++;
                    }
                    else
                    {
                        if (pages != maxpage)
                        {
                            ChangePage(pages + 1);
                            cursorPos = 0;
                        }
                    }
                }
                else if (deck == 2)
                {
                    if (cursorPos + 7 * reservePage >= sm.reserve.Count)
                    {
                        cursorPos = 0;
                        ChangePage(0);
                    }
                    else if (cursorPos < 6)
                    {
                        cursorPos++;
                    }
                    else
                    {
                        if (reservePage != maxreservePage)
                        {
                            ChangePage(reservePage + 1);
                            cursorPos = 0;
                        }
                    }
                }
            }

            if ((Input.GetButtonDown("Up") || Input.GetAxis("Vax") > 0.3f) && vMove)
            {
                vMove = false;

                if (deck == 1)
                {
                    if (cursorPos == 0)
                    {
                        if (pages > 0)
                        {
                            ChangePage(pages - 1);
                            cursorPos = 6;
                        }
                        else
                        {
                            ChangePage(maxpage);
                            pages = maxpage;
                            cursorPos = 6;
                        }
                    }
                    else
                    {
                        cursorPos--;
                    }
                }
                else if (deck == 2)
                {
                    if (cursorPos == 0)
                    {
                        if (sm.reserve.Count < 7)
                        {
                            cursorPos = sm.reserve.Count;
                        }
                        else if (reservePage > 0)
                        {
                            ChangePage(reservePage-1);
                            cursorPos = 6;
                        }
                        else
                        {
                            ChangePage(maxreservePage);
                            reservePage = maxreservePage;
                            var potato = Mathf.FloorToInt(sm.reserve.Count / 7);
                            cursorPos = sm.reserve.Count - potato * 7;
                        }
                    }
                    else
                    {
                        cursorPos--;
                    }
                }

            }

            if(deck ==1 && (Input.GetButtonDown("Right") || Input.GetAxis("Hax") > 0.3f))
            {
                ChangeSide(2);
            }
            if(deck==2 && (Input.GetButtonDown("Left") || Input.GetAxis("Hax") < -0.3f))
            {
                ChangeSide(1);
            }

            if(Input.GetKeyDown(KeyCode.R))
            {
                SelectPower();
            }

            ShowDescription();
        }
            if (Input.GetAxis("Vax") > -0.3f && Input.GetAxis("Vax") < 0.3f)
            {
                vMove = true;
            }

            cursorDeck.transform.localPosition = cursorPositions[cursorPos];

        foreach(Text texts in deckList)
        {
            texts.color = textColor;
        }

        if (targetName != null)
        {
            if((deck==1 && targetSide) || (deck==2 && !targetSide))
            {
                if((pages==targetPage) || (reservePage == targetPage))
                {
                    float t = Mathf.PingPong(Time.time, blinkSpeed) / blinkSpeed;
                    deckList[targetPos].color = Color.Lerp(textColor, transp, t);
                }
            }
        }
    }

    public void OpenDeck()
    {
        editBase.SetActive(true);
        deckBase.SetActive(true);
        cursorDeck.transform.localPosition = cursorBasePos;
        cursorPos = 0;
        deck = 1;
        pages = 0;
        reservePage = 0;
        maxpage = 3;
        maxreservePage = Mathf.FloorToInt(sm.reserve.Count / 7);


        ChangePage(0);
        
    }

    public void CloseDeck()
    {
        editBase.SetActive(false);
        deckBase.SetActive(false);
        reverseBase.SetActive(false);
        deck = 0;
    }

    public void Clearing()
    {
        audioS.clip = ouverture;
        audioS.Play();
        for (int i = 0; i < whichSelected.Length; i++)
        {
            if (whichSelected[i] != -1)
            {
                sm.cardsInHand[whichSelected[i]] = -1;
                whichSelected[i] = -1;
                selectedIcons[i].sprite = null;
            }
        }
        nextSelectedIcon = 0;
        sm.DrawHand();
        player.cards.Clear();
    }

    void Start()
    {
        //Permanent item (note : need to add the singleton thingy).
        DontDestroyOnLoad(gameObject);
        //Find objects, like always.
        sm = FindObjectOfType<ScrollManager>();
        gm = FindObjectOfType<GameManager>();
        //cursor = FindObjectOfType<Cursored>();
    }

    public void ChangeDisplay(int pos)
    {
        damage.text = sm.damages[sm.deck[sm.cardsInHand[pos]].spell];
        description.text = sm.description[sm.deck[sm.cardsInHand[pos]].spell];
        element1.sprite = sm.elements[sm.element1[sm.deck[sm.cardsInHand[pos]].spell]];
        element2.sprite = sm.elements[sm.element2[sm.deck[sm.cardsInHand[pos]].spell]];
        exemple.sprite = sm.exemples[sm.deck[sm.cardsInHand[pos]].spell];
        named.text = sm.names[sm.deck[sm.cardsInHand[pos]].spell];
    }

    public void ShowHand()
    {
        for(int i=0; i<10; i++)
        {
            if (i<sm.cardsInHand.Count)
            {
                hand[i].sprite = sm.icones[sm.deck[sm.cardsInHand[i]].spell];
                hand[i].color = Color.white;
                letters[i].sprite = sm.letters[sm.deck[sm.cardsInHand[i]].letter];
                letters[i].color = Color.white;
            }
            else
            {
                hand[i].sprite = null;
                hand[i].color = Color.clear;
                letters[i].sprite = null;
                letters[i].color = Color.clear;
            }
            
        }
    }

    public void Select(int pos)
    {
        if (nextSelectedIcon < selectedIcons.Length)
        {
            audioS.clip = valide;
            audioS.Play();
            selected[pos] = true;
            selectedIcons[nextSelectedIcon].sprite = hand[pos].sprite;
            hand[pos].color = Color.grey;
            letters[pos].color = Color.grey;
            whichSelected[nextSelectedIcon] = pos;
            nextSelectedIcon++;
            for (int i = 0; i < hand.Length; i++)
            {
                if (hand[i].sprite != null)
                {
                    if ((sm.deck[sm.cardsInHand[i]].letter != cursor.chosenLetter && sm.deck[sm.cardsInHand[i]].spell != cursor.chosenType) && sm.deck[sm.cardsInHand[i]].letter != 10)
                    {
                        hand[i].color = Color.black;
                        letters[i].color = Color.black;
                    }
                }

            }
        }
        else
        {
            audioS.clip = retour;
            audioS.Play();
        }
    }

    public void UnSelect()
    {
        audioS.clip = retour;
        audioS.Play();
        nextSelectedIcon--;
        int pos = whichSelected[nextSelectedIcon];
        
        selected[pos] = false;
        selectedIcons[nextSelectedIcon].sprite = null;
        hand[pos].color = Color.white;
        letters[pos].color = Color.white;
        whichSelected[nextSelectedIcon] = -1;

        if (whichSelected[0]==-1)
        {
            
                for (int i = 0; i < hand.Length; i++)
                {
                if (hand[i].sprite != null)
                {
                    hand[i].color = Color.white;
                    letters[i].color = Color.white;
                }
            }
        }
    }

    public void BackToGame()
    {
        for(int i=0; i<whichSelected.Length; i++)
        {
            if (whichSelected[i] != -1)
            {
                player.cards.Add(sm.cardsInHand[whichSelected[i]]);
            }
        }

        int indexed = 0;
        for (int i = 0; i < player.cards.Count; i++)
        {
            
            if (!sm.buff[sm.deck[player.cards[i]].spell])
            {
                
                player.hand[indexed].sprite = sm.icones[sm.deck[player.cards[i]].spell];
                
                player.hand[indexed].color = Color.white;
                player.handSquares[indexed].color = Color.white;
                indexed++;
            }
        }
        if (indexed < 6)
        {
            for (int i = indexed; i < player.hand.Length; i++)
            {
                player.hand[i].sprite = null;
                player.hand[i].color = Color.clear;
                player.handSquares[indexed].color = Color.clear;
            }
        }

        for (int i=0; i<selected.Length; i++)
        {
            selected[i] = false;
        }

        gm.BackCombat();
    }

}
