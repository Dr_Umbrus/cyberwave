﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileIdentity : MonoBehaviour
{
    public int posX;
    public int posY;
    public int side;
    public int tile;
    public GameManager gm;
    public bool canMove = false;
    public Grid grid;
    public float offset, offsetx;
    public bool destroyed = false;
    float recover;
    public GameObject trou;
    bool crack=false;
    Player potato;


    public void Awake()
    {
        grid = FindObjectOfType<Grid>();
        gm = FindObjectOfType<GameManager>();
        Startinging();
        Starting();
        potato = FindObjectOfType<Player>();
        if (trou != null)
        {
            trou.SetActive(false);
        }

    }

    public void Destroying()
    {
        if(potato.posX==posX && potato.posY == posY)
        {
            crack = true;
        }
        else
        {
            destroyed = true;
            recover = 10;
            trou.SetActive(true);
        }
        
    }

    private void Update()
    {
        if (recover > 0)
        {
            recover -= Time.deltaTime;
        }
        else
        {
            destroyed = false;
            trou.SetActive(false);
        }

        if (crack)
        {
            if(potato.posX != posX || potato.posY != posY)
            {
                destroyed = true;
                recover = 10;
                trou.SetActive(true);
                crack = false;
            }
            else
            {
                potato.Damaged(0, 1);
            }
        }
    }

    public void Startinging()
    {
        if (tile == 0)
        {

            gm.battleTerrain.Add(this);
            gm.battleEntities.Add(this);

        }

    }
    public virtual void Starting()
    {
        
    }

    private void OnDestroy()
    {
        gm.battleTerrain.Remove(this);
        if (tile <= 0)
        {
            gm.battleSol.Remove(this);
        }
        else
        {
            gm.battleEntities.Remove(this);
        }
    }
}
