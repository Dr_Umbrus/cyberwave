﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BroDis : Bro
{

    public GameObject projectile;
    bool ready = false;

    public override void Starting2()
    {
        startTimeAttack += Random.Range(-10, 11) / 10;
        timeAttack = startTimeAttack;
    }
    public override void Pattern()
    {

        if (!stopped)
        {

            if (!stopped)
            {

                if (timeMove > 0)
                {
                    timeMove -= Time.deltaTime;
                }
                else
                {
                    int x = 0;
                    int y = 0;

                    canMove = false;
                    while (!canMove)
                    {
                        x = Random.Range(0,2);
                        y = Random.Range(0, 4);

                        canMove = gm.CheckMove(x, y, 0, 0, 2);
                        if (x == posX && y == posY)
                        {
                            canMove = false;
                        }
                    }

                    posX = x;
                    posY = y;
                    transform.position = gm.spawnPosL[4 * (x) + y];
                    transform.Translate(new Vector3(0, offset, -5));
                    timeMove = startTimeMove;

                }
            }
        }

        if (timeAttack > 0 && !stopped && !ready)
        {
            timeAttack -= Time.deltaTime;
        }

        if (timeAttack <= 0)
        {
            timeAttack = startTimeAttack;
            anim.SetTrigger("Attacking");
            stopped = true;
            attacking = true;
        }

        if (sr.sprite == attackSprite && attacking)
        {
            ready = true;
            
            
        }

        if (ready)
        {
            var shot = false;
            for (int i = 0; i < gm.enemiesLeft.Count; i++)
            {
                if (gm.enemiesLeft[i].posY == posY && !shot)
                {
                    shot = true;
                    GameObject temp = Instantiate(projectile, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                    temp.GetComponent<BroShot>().SetStats(posX, posY, 1, gm);
                    attacking = false;
                }
            }
            ready = false;
        }
    }

    void CloseIn()
    {
        if (posY == 1 || posY == 3)
        {
            canMove = gm.CheckMove(posX, posY, 1, -1, 1);
            if (canMove)
            {
                timeMove = startTimeMove;
                posY--;
                transform.Translate(new Vector3(0, -1.1f, 0));
            }
        }
        else
        {
            canMove = gm.CheckMove(posX, posY, 1, 1, 1);
            if (canMove)
            {
                timeMove = startTimeMove;
                posY++;
                transform.Translate(new Vector3(0, 1.1f, 0));
            }
        }
    }

    void MoveIn()
    {
        if (posX == 7 || posX == 5)
        {
            canMove = gm.CheckMove(posX, posY, 1, -1, 0);
            if (canMove)
            {
                timeMove = startTimeMove;
                posX--;
                transform.Translate(new Vector3(-2.15f, 0, 0));
            }

        }
        else if (posX == 6 || posX == 4)
        {

            canMove = gm.CheckMove(posX, posY, 1, 1, 0);
            if (canMove)
            {
                timeMove = startTimeMove;
                posX++;
                transform.Translate(new Vector3(2.15f, 0, 0));
            }
        }
    }
}
