﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Press : MonoBehaviour
{
    GameManager gm;
    Player player;
    float timeAttack;
    int posX, posY;
    public Vector3 offset;
    bool pressed = false;
    bool acquired = false;
    bool stomped = false;
    float y, endY;
    public List<SpriteRenderer> targetCases;
    public GameObject souffle;

    void Start()
    {
        player = FindObjectOfType<Player>();
        gm = FindObjectOfType<GameManager>();
        timeAttack = Random.Range(15, 31) / 10;
    }

    
    void Update()
    {
        if (timeAttack > 0)
        {
            timeAttack -= Time.deltaTime;
            transform.position = player.transform.position + offset;
            posX = player.posX;
            posY = player.posY;
            y = transform.position.y;
        }

        else
        {
            if (!acquired)
            {
                for(int i=0; i<gm.battleSol.Count; i++)
                {
                    if(gm.battleSol[i].posX==posX && gm.battleSol[i].posY == posY)
                    {
                        endY = gm.battleSol[i].transform.position.y;
                    }

                    if (gm.battleSol[i].posX < posX + 2 && gm.battleSol[i].posY < posY + 2 && gm.battleSol[i].posX > posX - 2 && gm.battleSol[i].posY > posY - 2)
                    {
                        targetCases.Add(gm.battleSol[i].GetComponent<SpriteRenderer>());
                    }
                }

                foreach(SpriteRenderer cases in targetCases)
                {
                    cases.color = Color.red;
                }
                
                acquired = true;
            }
            if (!pressed)
            {
                transform.Translate(new Vector3(0, 5, 0) * Time.deltaTime);
                if (transform.position.y > y + 3.5f)
                {
                    pressed = true;
                }
            }
            else if(!stomped)
            {
                transform.Translate(new Vector3(0, -20, 0) * Time.deltaTime);
                if (transform.position.y <= endY)
                {
                    GetComponent<AudioSource>().Play();
                    transform.position = new Vector3(transform.position.x, endY, transform.position.z);
                    Destroy(Instantiate(souffle, transform.position, Quaternion.identity), 0.5f);
                    stomped = true;
                    foreach (SpriteRenderer cases in targetCases)
                    {
                        cases.color = Color.white;
                    }
                }
            }
            else
            {
                transform.Translate(new Vector3(0, 30, 0) * Time.deltaTime);
                if (transform.position.y > 10)
                {
                    Destroy(gameObject);
                }
            }

        }
    }
}
