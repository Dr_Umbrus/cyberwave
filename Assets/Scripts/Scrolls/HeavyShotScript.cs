﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyShotScript : Scrolls
{
    public GameObject Shot;
    public Player playerScript;
    bool ok = false;
    int dakkad;
    float stune;


    public override void Starting()
    {
            playerScript = FindObjectOfType<Player>();
    }

    public override void Casting(int dakka, float stun)
    {
        dakkad = dakka;
        stune = stun;
        playerScript.stopped = true;
        playerScript.anim.SetTrigger("Attacking");
        playerScript.attacking = true;
        playerScript.anim.speed = 1.5f;
        ok = true;
        

    }

    private void Update()
    {
        if (playerScript.sr.sprite == playerScript.attackSprite && ok)
        {
            playerScript.attacking = false;
            ok = false;
            GameObject temp = Instantiate(Shot, player.transform.position, Quaternion.identity);
            temp.GetComponent<Projectile>().MoreDakka(dakkad, stune);
        }

        if (playerScript.myState == PlayerState.Stun)
        {
            StopAllCoroutines();
            playerScript.stopped = false;
        }
    }

}
