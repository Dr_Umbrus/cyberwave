﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRS : Enemies
{
    public GameObject trap;
    public GameObject gurnade;
    public GameObject lBD;
    public GameObject arest;

    public CRS friend=null;
    public CRS lead=null;
    public bool leading;

    public override void Starting2()
    {
        if (!leading)
        {
            startTimeAttack += Random.Range(-5, 21) / 10;
            timeAttack = startTimeAttack;
            timeAttack += Random.Range(-5, 11) / 10;
        }
        

        if (leading)
        {
            StartCoroutine(GetFriend());
        }
        
        
    }

    IEnumerator GetFriend()
    {
        int trying= 90;
        CRS[] potato = FindObjectsOfType<CRS>();
        while (friend == null || trying>0)
        {
            yield return 0;
            if (potato.Length > 0)
            {
                foreach (CRS cop in potato)
                {
                    if (cop.posX == posX && cop != this)
                    {
                        friend = cop;
                    }
                }
            }
            trying--;
        }
        if (friend != null)
        {
            friend.Leader(this);
            startTimeAttack = friend.startTimeAttack;
            timeAttack = friend.timeAttack;
        }
    }
    public void Leader(CRS cop)
    {
        lead = cop;
    }

    public void Follow(bool what)
    {
        if (what)
        {
            CloseIn();
        }
        else
        {
            MoveIn();
        }

    }

    void CloseIn()
    {
        if(posY==1 || posY == 3)
        {
            canMove = gm.CheckMove(posX, posY, 1, -1, 1);
            if (canMove)
            {
                timeMove = startTimeMove;
                posY--;
                transform.Translate(new Vector3(0, -1.1f, 0));
            }
        }
        else
        {
            canMove = gm.CheckMove(posX, posY, 1, 1, 1);
            if (canMove)
            {
                timeMove = startTimeMove;
                posY++;
                transform.Translate(new Vector3(0, 1.1f, 0));
            }
        }
    }

    void MoveIn()
    {
        if (posX == 7 || posX==5)
        {
                canMove = gm.CheckMove(posX, posY, 1, -1, 0);
                if (canMove)
                {
                    timeMove = startTimeMove;
                    posX--;
                    transform.Translate(new Vector3(-2.15f, 0, 0));
                }

        }
        else if (posX == 6 || posX==4)
        {
            
                canMove = gm.CheckMove(posX, posY, 1, 1, 0);
                if (canMove)
                {
                    timeMove = startTimeMove;
                    posX++;
                    transform.Translate(new Vector3(2.15f, 0, 0));
                }
        }
    }

    public override void Pattern()
    {
        
        if (!stopped)
        {
            if (lead == null)
            {

                if (timeMove > 0)
                {
                    timeMove -= Time.deltaTime;
                }
                else
                {
                    /*if (posX < 6)
                    {
                        if (player.posY > posY)
                        {
                            canMove = gm.CheckMove(posX, posY, 1, 1, 1);
                            if (canMove)
                            {
                                timeMove = startTimeMove;
                                posY++;
                                transform.Translate(new Vector3(0, 1.1f, 0));
                            }
                        }
                        if (player.posY < posY)
                        {
                            canMove = gm.CheckMove(posX, posY, 1, -1, 1);
                            if (canMove)
                            {
                                timeMove = startTimeMove;
                                posY--;
                                transform.Translate(new Vector3(0, -1.1f, 0));
                            }
                        }
                    }
                    else
                    {*/
                        int moveChoice = Random.Range(0, 100);
                        if (moveChoice < 50)
                            {
                            MoveIn();
                            }
                            else
                            {
                                CloseIn();
                            }
                        if (friend != null)
                        {
                            if (moveChoice < 50)
                            {
                                friend.Follow(false);
                            }
                            else
                            {
                                friend.Follow(true);
                            }
                        //}
                    }
                    
                }
            }
        }

        if (timeAttack > 0 && !stopped)
        {
            timeAttack -= Time.deltaTime;
        }

        if (timeAttack <= 0)
        {
            timeAttack = startTimeAttack;
            anim.SetTrigger("Attacking");
            stopped = true;
            attacking = true;
        }

        if (sr.sprite == attackSprite && attacking)
        {
            if (lead == null)
            {
                var rand = Random.Range(0, 100);

                Attack(rand);
                if (friend != null)
                {
                    friend.Attack(rand);
                }
            }
        }

    }

    public void Attack(int rand)
    {
        if (posX > 5)
        {
            if (posY == 0 || posY == 3)
            {
                if (rand < 35)
                {
                    GameObject temp = Instantiate(lBD, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                }
                else if (rand < 60)
                {
                    GameObject temp = Instantiate(gurnade, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                    temp.GetComponent<Lob>().SetStats(posX, posY);
                }
                else
                {
                    if (trap != null)
                    {
                        Destroy(trap);
                    }
                    trap = null;

                    if (lead == null)
                    {
                        GameObject temp = Instantiate(arest, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                        temp.GetComponent<Arrest>().father = this;
                        trap = temp;
                    }
                    else
                    {
                        GameObject temp = Instantiate(arest, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                        temp.GetComponent<Arrest>().father = this;
                        trap = temp;

                        var x = Random.Range(0, 2);
                        var y = Random.Range(0, 2);
                        if (x == 0)
                        {
                            x = -1;
                        }
                        if (y == 0)
                        {
                            y = -1;
                        }

                        trap.GetComponent<Arrest>().Offset(x, y);
                    }
                    
                }
            }
            else
            {
                if (rand < 50)
                {
                    GameObject temp = Instantiate(lBD, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                }
                else
                {
                    GameObject temp = Instantiate(gurnade, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                    temp.GetComponent<Lob>().SetStats(posX, posY);
                }
            }
        }
        else if (posY == 0 || posY == 3)
        {
            if (rand < 50)
            {
                if (trap != null)
                {
                    Destroy(trap);
                }
                trap = null;
                if (lead == null)
                {
                    GameObject temp = Instantiate(arest, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                    temp.GetComponent<Arrest>().father = this;
                    trap = temp;
                }
                else
                {
                    GameObject temp = Instantiate(arest, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
                    temp.GetComponent<Arrest>().father = this;
                    trap = temp;

                    var x = Random.Range(0, 2);
                    var y = Random.Range(0, 2);
                    if (x == 0)
                    {
                        x = -1;
                    }
                    if (y == 0)
                    {
                        y = -1;
                    }

                    trap.GetComponent<Arrest>().Offset(x, y);
                }
            }
            else
            {
                GameObject temp = Instantiate(lBD, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
            }
        }
        else
        {
            GameObject temp = Instantiate(lBD, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
        }

        attacking = false;
    }

    private void OnDestroy()
    {
        if (friend != null)
        {
            friend.lead = null;
        }
    }
}
