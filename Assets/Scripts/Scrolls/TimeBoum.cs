﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBoum : TileIdentity
{
    public int damage;
    public float paraTime;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void MoreDakka(int dakka, float dur)
    {
        damage += dakka;
        paraTime += dur;

    }

    private void OnDestroy()
    {
        for (int i = 0; i < gm.battleEntities.Count; i++)
        {
            if(gm.battleEntities[i].posX>posX-2 && gm.battleEntities[i].posX < posX +2 && gm.battleEntities[i].posY > posY-2 && gm.battleEntities[i].posY < posY + 2)
            {
                if(gm.battleEntities[i].GetComponent<Enemies>()!=null)
                gm.battleEntities[i].GetComponent<Enemies>().Damaged(damage, paraTime);
            }
        }
    }
}
