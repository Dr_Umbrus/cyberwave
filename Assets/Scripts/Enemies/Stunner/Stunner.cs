﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stunner : Enemies
{
    public GameObject circleStun;
    public Player playerScript;
    public bool a, b, c, d;

    public override void Starting2()
    {
        playerScript = FindObjectOfType<Player>();
    }

    public override void Pattern()
    {


        if (!stopped)
        {
            if (timeMove > 0)
            {
                timeMove -= Time.deltaTime;
            }
            else
            {
                canMove = false;
                timeMove = startTimeMove;
                timeAttack--;
                int move = -2;
                a = b = c = d = false;
                
                while (!canMove)
                {
                    if(a && b && c && d)
                    {
                        move = -1;
                        canMove = true; ;
                    }
                    else
                    {
                        move = Random.Range(0, 4);
                    }

                    if (move == 0)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, -1, 0);
                        if (!canMove)
                        {
                            a = true;
                        }
                    }
                    if (move == 1)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, 1, 0);
                        if (!canMove)
                        {
                            b = true;
                        }
                    }
                    if (move == 2)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, 1, 1);
                        if (!canMove)
                        {
                            c = true;
                        }
                    }
                    if (move == 3)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, -1, 1);
                        if (!canMove)
                        {
                            d = true;
                        }
                    }
                }

                if (move == 0)
                {
                    posX--;
                    transform.Translate(new Vector3(-2.15f, 0, 0));
                }
                if (move == 1)
                {
                    posX++;
                    transform.Translate(new Vector3(2.15f, 0, 0));
                }
                if (move == 2)
                {
                    posY++;
                    transform.Translate(new Vector3(0, 1.1f, 0));
                }
                if (move == 3)
                {
                    posY--;
                    transform.Translate(new Vector3(0, -1.1f, 0));
                }

                canMove = false;
            }

            if (timeAttack <= 0)
            {
                stopped = true;
                attacking = true;
                timeAttack = Random.Range(2, 5);
                anim.SetTrigger("Attacking");
                if (playerScript != null)
                {
                    GameObject temp = Instantiate(circleStun, playerScript.transform.position, Quaternion.identity);
                    temp.GetComponent<Projectile>().SetStats(playerScript.posX, playerScript.posY, 0, gm);
                    temp.GetComponent<StunningShot>().Father(this);
                }
                

            }
        }

        if (sr.sprite == attackSprite && attacking)
        {
            
            attacking = false;
        }

    }

}
