﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollManager : MonoBehaviour
{
    #region properties
    /*Note : Each spell must have a unique ID number
     and be at said number's position in all arrays/lists */
     [Header("Scroll Properties")]
    //Scrolls names
    public string[] names;
    //Scrolls description
    public string[] description;
    //Script for casting the scroll
    public Scrolls[] scripts;

    public string[] damages;
    public Sprite[] elements;
    public int[] element1;
    public int[] element2;
    public Sprite[] letters;

    public Sprite[] icones;
    public Sprite[] exemples;

    public bool[] buff;
    public int[] weight;
    

    #endregion

    #region deck and combat
    /*The content of the deck. Each int in "scrolls" is the id of a scroll in the deck.
    For instance, a deck can be a list of 0,2,1,2,0, meaning we have twice the spells with id 0 et 2, and once the spell with id 1.*/
    [Header("Combat deck")]

    public List<ScrollStats> deck = new List<ScrollStats>();

    public List<ScrollStats> reserve = new List<ScrollStats>();

    public int cardsInDeck = 0;

    /*public List<int> scrolls;
    //The associated letter
    public List<int> scrollLetter;*/

    

    //Remaining scrolls in the deck. Used in combat.
    public List<int> remaining;
    //Scrolls in the discard pile.
    public List<int> discarded;
    //Scrolls currently in your hand.
    public List<int> cardsInHand;
    //The max number of scrolls in the hand.
    public int handSize = 5;
    
    
    //What scrolls of the deck already haven't been drawn yet (see code for explanation)
    public List<int> notPick;

    public int maxDeck;
    #endregion

    public VisualManager vm;
   
    //Singleton
    private GameObject _instance;

    public void SummonScrolls()
    {
        foreach(Scrolls scro in scripts)
        {
            scro.PlayerSearch();
            scro.Starting();
        }
    }

    void Awake()
    {
        List<int> resultingChoices = new List<int>();
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this.gameObject;
        }
        if (deck.Count == 0)
        {

            for (int i = 0; i < weight.Length; i++)
            {
                for (int j = 0; j < weight[i]; j++)
                {
                    resultingChoices.Add(i);
                }
            }



            ///Version building
            for(int i=0; i < scripts.Length; i++)
            {
                for (int j = 0; j < scripts[i].letters.Length; j++)
                {
                    ScrollStats added = new ScrollStats();
                    added.spell = i;
                    added.letter = scripts[i].letters[j];
                    added.ammount = 3;
                    reserve.Add(added);
                }
                

            }
            ///Version random
            /*int randomSpell;
            for (int i = 0; i < 30; i++)
            {
                randomSpell = Random.Range(0, resultingChoices.Count);
                randomSpell = resultingChoices[randomSpell];
                int randomLetter= scripts[randomSpell].letters[Random.Range(0, scripts[randomSpell].letters.Length)];
                bool notAlready = true ;

                if (reserve.Count != 0)
                {

                    for (int j = 0; j < reserve.Count; j++)
                    {
                        if (randomSpell == reserve[j].spell && randomLetter == reserve[j].letter)
                        {
                            reserve[j].ammount++;
                            notAlready = false;
                        }
                    }
                }

                    if (notAlready)
                    {
                        ScrollStats added = new ScrollStats();
                        added.spell = randomSpell;

                        added.letter = randomLetter;
                        added.ammount = 1;
                        reserve.Add(added);
                    }
            }*/
            

            ///Version random no deck
            int randomSpell;
             for (int i = 0; i < 14; i++)
            {
                
                randomSpell = Random.Range(0, resultingChoices.Count);
                randomSpell = resultingChoices[randomSpell];
                ScrollStats adding = new ScrollStats();
                adding.spell = randomSpell;

                adding.letter= scripts[randomSpell].letters[Random.Range(0, scripts[randomSpell].letters.Length)];
                adding.ammount = 1;
                deck.Add(adding);
            }


        }
    }

    //What happens here when we cast a spell. id is the ID of the spell. Card is for the hand.
    public void Cast(int id, int dakka, float stun)
    {
        //It takes the spells' script in the list, and calls for his Casting function (see inheritance)
        scripts[id].Casting(dakka, stun);

        //The spell is discarded.
        discarded.Add(deck[id].spell);
        //cardsInHand[cardID] = -1;
    }


    //How to add a scroll to the deck.
    public void AddScrolls(int id, bool fix, int letter)
    {
        ScrollStats added = new ScrollStats();
        if (fix)
        {
            
            added.spell = id;
            added.letter = letter;
            deck.Add(added);
        }
        else
        {
            added.spell = id;
            added.letter = scripts[id].letters[Random.Range(0, scripts[id].letters.Length)];
            deck.Add(added);
        }
        
    }

    //How to remove a scroll from the deck.
    public void RemoveScrolls(int position)
    {
        deck.RemoveAt(position);
    }

    //Test function to see the content of the deck. I'll find a way to do it, someday.
    public void ShowScrolls()
    {
        
    }

    //When a fight start.
    public void CombatStart()
    {

        //Checks if cardsInHand matches the max hand size and corrects it. Usefull if the player can extend said hand size.
        if (cardsInHand.Count < handSize)
        {
            for(int i=0; i<handSize-cardsInHand.Count; i++)
            {
                cardsInHand.Add(-1);
            }
        }
        else if (cardsInHand.Count > handSize)
        {
            cardsInHand.Clear();
            for (int i = 0; i <handSize; i++)
            {
                cardsInHand.Add(-1);
            }
        }

        //Makes sure the draw pile is the same as the deck.
        remaining.Clear();
        for (int i = 0; i < deck.Count; i++)
        {
            remaining.Add(deck[i].spell);
        }
        
        

        notPick.Clear();
        //Fills the 'notPick' with all the scrolls positions of 'remaining', for further use.
        for (int i=0; i<remaining.Count; i++)
        {
            notPick.Add(i);
        }
        //Then we draw.
        DrawHand();
    }


    public void Newwave()
    {
        for(int i=0; i<scripts.Length; i++)
        {
            scripts[i].StopAllCoroutines();
        }
    }

    public void DrawHand()
    {
        
        if (cardsInHand.Count == 0)
        {
            for (int i = 0; i < handSize; i++)
            {
                cardsInHand.Add(-1);
            }
            remaining.Clear();
            for (int i = 0; i < deck.Count; i++)
            {
                remaining.Add(deck[i].spell);
            }
        }

        for(int i=0; i<handSize; i++)
        {
            if (cardsInHand[i] == -1)
            {
                bool test = false;

                for(int j=i; j<handSize; j++)
                {
                    if (cardsInHand[j] != -1 && !test)
                    {
                        test = true;
                        cardsInHand[i] = cardsInHand[j];
                        cardsInHand[j] = -1;
                    }
                }
            }
        }

        //Loop until the  hand is full.
        for (int i = 0; i < handSize; i++)
        {
            //If all the deck has already been drawn
            if (notPick.Count == 0)
            {
                //The discard pile is shuffled in the draw pile.
                Reshuffle();
            }

            if (cardsInHand[i] == -1)
            {
                //We pick a random number that wasn't already drawn.
                int randomized = Random.Range(0, notPick.Count);
                //The card stocks the number à the randomized position, in 'notPick'.
                //This means, the card holds the position in the 'remaining' list of the scroll to use.
                cardsInHand[i] = notPick[randomized];
                while (cardsInHand[i] == -1)
                {
                    notPick.RemoveAt(randomized);
                    randomized = Random.Range(0, notPick.Count);
                    cardsInHand[i] = notPick[randomized];
                }
                //The drawn scroll is then removed from the pool.
                notPick.RemoveAt(randomized);
            }
        }
        vm.ShowHand();
    }

    //When the deck is empty and you have to fill it again.
    public void Reshuffle()
    {
        //Security check that 'notPick' is empty. Useful if we want to have shuffling spells.
        if (notPick.Count > 0)
        {
            notPick.Clear();
        }

        //we reset the 'notPick' list of positions.
        for(int i=0; i<remaining.Count; i++)
        {
            notPick.Add(i);
        }

        //We remove from the pool every scroll that is still in hand.
        for(int i=0; i<handSize; i++)
        {
            if (cardsInHand[i] != -1)
            {
                notPick[cardsInHand[i]]=-1;
            }
        }
        
        /*for(int i=notPick.Count-1; i>-1; i--)
        {
            if (notPick[i] == -1)
            {
                notPick.Remove(i);
            }
        }*/
        
       /* //Every exhausted scroll is removed from the pool.
        if (exhausted.Count > 0)
        {
            for (int i = 0; i < exhausted.Count; i++)
            {
                notPick.RemoveAt(exhausted[i]);
            }
        }*/
        
        //The discard pile is completely removed.
        discarded.Clear();  
    }

    /*public void StopCombat()
    {
        for(int i=0; i<cardsInHand.Count; i++)
        {
            cardsInHand[i] = -1;
            handVisual[i].sprite = null;
            handVisual[i].gameObject.SetActive(false);
        }
    }*/
}
