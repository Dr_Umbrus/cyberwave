﻿Shader "Custom/Trail"
{
	Properties
	{
		_InsideFarColor("Inside Far Color", Color) = (0.2,0.2,1,1)
		_InsideNearColor("Inside Near Color", Color) = (0.5,1,1,1)
		_MainTex("pixel",2D) = "white" {}
	_Pixellisation("transp", float) = 1
		_PixelFactor("factor", float) = 1
	}
		SubShader
	{
		Tags { "Queue" = "Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha
		Pass{
		CGPROGRAM

#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

	fixed4 _OutsideColor;
	fixed4 _InsideFarColor;
	fixed4 _InsideNearColor;
	fixed _Pixellisation;
	fixed _PixelFactor;
	sampler2D _MainTex;
	float4 _MainTex_ST;


		struct appdata {
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
		float4 color: COLOR;

	};

	struct v2f {
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
		float4 color : COLOR;
	};

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = float4(v.uv.xy, 0, 0);
		o.color = float4(0, 0, v.uv.y, 1);
		return o;
	}
	float nrand(float2 uv) {
		return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453)*_PixelFactor;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		float distY = abs(i.uv.y - .5) * 2;
	fixed4 c;
		c = lerp(_InsideNearColor, _InsideFarColor, distY);

	if (nrand(i.vertex) > _Pixellisation - (i.uv.x * i.uv.x))
	{
		c = float4(0, 0, 0, 0); 
	}
	return c;
	}
		ENDCG
		}
	}

}