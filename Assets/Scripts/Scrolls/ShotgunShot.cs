﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunShot : Projectile
{
    public GameObject shrapnel;
    public List<SpriteRenderer> targetCases;
    public List<GameObject> hit;

    private void Start()
    {
        for (int i=0; i<45; i++)
        {
            GameObject temp=Instantiate(shrapnel, new Vector3(transform.position.x-2.1f, transform.position.y, transform.position.z), Quaternion.identity);
            temp.GetComponent<Projectile>().Started();
            Destroy(temp, 0.2f);
        }

        for (int i = 0; i < gm.battleSol.Count; i++)
        {
            
            if ((gm.battleSol[i].posX == posX && gm.battleSol[i].posY == posY) || (gm.battleSol[i].posX == posX+ 1 && gm.battleSol[i].posY > posY - 2 && gm.battleSol[i].posY < posY+2) || (gm.battleSol[i].posX==posX+2 && gm.battleSol[i].posY>posY-3 && gm.battleSol[i].posY <posY + 3))
            {
                targetCases.Add(gm.battleSol[i].GetComponent<SpriteRenderer>());
            }
        }
        foreach(SpriteRenderer cases in targetCases)
        {
            cases.color = Color.blue;
        }
        Destroy(gameObject, 0.5f);
        Shot();
    }

    private void Shot()
    {
        for(int i=0; i<gm.battleEntities.Count; i++)
        {
            
            if (gm.battleEntities[i].side==1)
            {
                bool targets = false;
                for (int j = 0; j < 3; j++)
                {
                    if (gm.battleEntities[i].posX == posX+j && gm.battleEntities[i].posY>=posY-j && gm.battleEntities[i].posY <= posY + j)
                    {
                        targets = true;
                    }
                }
                if (targets)
                {
                    bool test = true;
                    if (hit.Count > 0)
                    {
                        for (int j = 0; j < hit.Count; j++)
                        {
                            if (gm.battleEntities[i].gameObject == hit[j])
                            {
                                test = false;
                            }
                        }
                    }
                    if (test)
                    {
                        gm.battleEntities[i].GetComponent<Enemies>().Damaged(damage, paraTime);
                        if (gm.battleEntities[i].GetComponent<Enemies>().life > 0)
                        {
                            hit.Add(gm.battleEntities[i].gameObject);
                        }
                    }
                }
                
                
            }
        }
    }


    private void OnDestroy()
    {
        foreach (SpriteRenderer cases in targetCases)
        {
            cases.color = Color.white;
        }
    }
}
