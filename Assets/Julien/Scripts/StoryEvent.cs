﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryEvent : MonoBehaviour
{
    public GameObject ancestor;
    //Ennemis/Entités affectés par les events
    public GameObject captain;
    public GameObject flic;
    public GameObject[] renforts;
    public GameObject bro1, bro2;

    //Texte
    public Text dialog;

    public int eventStoryNumber = 0;
    public int eventNumber=1;

    //détermine si en cours de séquence narrative ou non , détermine quand des séquences scriptés sont en cours pour éviter de passer tout les dialogues
    private bool cinematic = false;
    private bool cIsPlaying = false;

    //Varibale texte fluide, array pour les textes des dialogues , int pour la phrase de l array a display , bool pour vérif que toute la phrase a été display
    string[] goatText = new string[] { "Tu t'arrêtes ici!", "Restitue nous les données maintenant, et j'en finirais vite.", "Jamais ! Ces données sont nécessaire pour sauver des vies.", "Alors prépare-toi aux conséquences.", "Impossible, comment nos proxy ont-ils pu être aussi inefficace ?", "Je n'ai pas de temps à perdre , écarte-toi.", "Vous la bas, arrêtez-vous!", "Tss, ne crois pas qu'on s'arrêtera là.", "Toi là ! Tu vas nous suivre sans faire d'histoire.", "Ca ne va pas être possible.", "Agents, en formation , arrêtez-là... par n'importe quel moyen.", "Ah...cétait limite", "Tu n'échapperas pas...à la loi", "Tu n'as plus d'échappatoire , rends-toi!", "Là, ça va être compliqué", "N'abandonne pas maintenant!", "Tu dois continuer , trop de personnes en dépendent", "Relève toi , et battons nous pour eux." };
    private int currentlyDisplayingText = 0;
    private float displaySpeed = 0.08f;
    private bool textIsComplete;

    //Variables Gestion sprite portait , la liste est publique -> a remplir dans l'éditeur , contient les sprites des différents perso qui interviennent dans les dialogues, appeler la variable id correspondant au perso qui parle
    [SerializeField]
    public class EmotionEntry
    {
        public string id;
        public Sprite sprite;
    }
    public List<Sprite> characterSprites;
    public Image dialogChar;

    GameManager gm;

    private void Start()
    {
        gm = GetComponent<GameManager>();
        EndDial();
    }

    public void EndDial()
    {
        ancestor.SetActive(false);
        dialog.text = "";
    }

    public void StartDial()
    {
        ancestor.SetActive(true);
    }

    void Update()
    {
        /*//désactivation move joueur pendant event
        if (cinematic == true) { player.GetComponent<PlayerOverworld>().enabled = false; }
        else { player.GetComponent<PlayerOverworld>().enabled = true; }*/

        if (cinematic)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (textIsComplete && !cIsPlaying)
                {
                    StartCoroutine(NextEvent());
                    currentlyDisplayingText++;
                }
                else
                {
                    displaySpeed = 0;
                }
            }
        }
    }

    public void InitiateStory()
    {
        StartDial();
        cinematic = true;
        gm.played.stopped = true;
        //dialog.GetComponent<UnityEngine.UI.Text>().color = Color.red;
        //dialog.gameObject.SetActive(true);
        StartCoroutine(AnimateText());

    }



    IEnumerator NextEvent()
    {
        eventStoryNumber++;

        if (eventNumber == 1)
        {

            if (eventStoryNumber == 1)
            {
                //Arrivée des ennemis face aux joueurs
                cIsPlaying = true;
                dialog.gameObject.SetActive(false);
                captain.SetActive(true);
                var t2 = 0f;
                var timeToMove = 1f;

                yield return new WaitForSeconds(0.5f);
                while (t2 < 1)
                {
                    t2 += Time.deltaTime / timeToMove;

                    captain.gameObject.transform.position = Vector3.Lerp(captain.gameObject.transform.position, new Vector3(captain.transform.position.x - 0.07f, captain.transform.position.y, captain.transform.position.z), t2);
                    yield return 0;
                }
                //dialogChar.gameObject.SetActive(true);
                dialog.gameObject.SetActive(true);
                StartCoroutine(AnimateText());
                //dialog.GetComponent<UnityEngine.UI.Text>().text = "Remet nous l'objet en ta possession";
                //dialog.GetComponent<UnityEngine.UI.Text>().color = Color.red;
                dialogChar.sprite = characterSprites[1];
                cIsPlaying = false;

            }
            //réponse joueur
            if (eventStoryNumber == 2)
            {
                cIsPlaying = true;
                //dialog.GetComponent<UnityEngine.UI.Text>().text = "Vous pouvez courir";
                StartCoroutine(AnimateText());
               // dialog.GetComponent<UnityEngine.UI.Text>().color = Color.blue;
                dialogChar.sprite = characterSprites[0];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            //réponse ennemi
            if (eventStoryNumber == 3)
            {
                cIsPlaying = true;
                //dialog.GetComponent<UnityEngine.UI.Text>().text = "alors décède";
                StartCoroutine(AnimateText());
                //dialog.GetComponent<UnityEngine.UI.Text>().color = Color.red;
                dialogChar.sprite = characterSprites[1];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 4)
            {
                cIsPlaying = true;
                /* dialog.gameObject.SetActive(false);
                 dialogChar.gameObject.SetActive(false);*/
                //passage en mode combat
                cIsPlaying = false;
                gm.PrepareBattle(1);
                captain.SetActive(false);
                eventNumber++;
                eventStoryNumber = 1;
            }
        }

        else if (eventNumber == 2)
        {
            
            if (eventStoryNumber == 1)
            {
                captain.SetActive(true);
                cIsPlaying = true;
                
                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[1];
                cIsPlaying = false;

            }
            //réponse joueur
            if (eventStoryNumber == 2)
            {
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[0];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            //réponse ennemi
            if (eventStoryNumber == 3)
            {
                cIsPlaying = true;
                
                StartCoroutine(AnimateText());

                dialogChar.sprite = null;
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 4)
            {
                cIsPlaying = true;

                dialog.gameObject.SetActive(false);
                flic.SetActive(true);
                var t = 0f;
                var timeToMove = 1f;

                yield return new WaitForSeconds(0.5f);
                while (t < 1)
                {
                    t += Time.deltaTime / timeToMove;

                    flic.gameObject.transform.position = Vector3.Lerp(flic.gameObject.transform.position, new Vector3(flic.transform.position.x, flic.transform.position.y + 0.07f, flic.transform.position.z), t);
                    yield return 0;
                }


                dialog.gameObject.SetActive(true);
                StartCoroutine(AnimateText());
                dialogChar.sprite = characterSprites[1];
                cIsPlaying = false;
                
            }
            if (eventStoryNumber == 5)
            {
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[2];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 6)
            {
                var t = 0f;
                var timeToMove = 1f;

                while (t < 1)
                {
                    t += Time.deltaTime / timeToMove;

                    captain.gameObject.transform.position = Vector3.Lerp(captain.gameObject.transform.position, new Vector3(captain.transform.position.x + 0.07f, captain.transform.position.y, captain.transform.position.z), t);
                    yield return 0;
                }
                captain.GetComponent<Appearance>().Death();
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[0];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 7)
            {
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[2];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 8)
            {
                flic.SetActive(false);
                gm.PrepareBattle(2);
                eventNumber++;
                eventStoryNumber = 1;
            }
        }

        else if (eventNumber == 3)
        {
            if (eventStoryNumber == 1)
            {
                flic.SetActive(true);
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[0];
                cIsPlaying = false;

            }
            //réponse joueur
            if (eventStoryNumber == 2)
            {
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[2];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            //réponse ennemi
            if (eventStoryNumber == 3)
            {
                flic.GetComponent<Appearance>().Death();
                cIsPlaying = true;
                dialog.gameObject.SetActive(false);
                foreach (GameObject bruh in renforts)
                {
                    bruh.SetActive(true);
                }
                /*var t3 = 0f;
                var timeToMove = 1f;
                while (t3 < 1)
                {
                    t3 += Time.deltaTime / timeToMove;

                    for (int i = 0; i < renforts.Length; i++)
                    {
                        renforts[i].gameObject.transform.position = Vector3.Lerp(renforts[i].gameObject.transform.position, new Vector3(renforts[i].transform.position.x, renforts[i].transform.position.y + 0.07f, renforts[i].transform.position.z), t3);
                    }
                    
                    yield return 0;
                }*/
                yield return new WaitForSeconds(0.5f);
                dialog.gameObject.SetActive(true);
                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[2];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 4)
            {
                cIsPlaying = true;

                
                StartCoroutine(AnimateText());
                dialogChar.sprite = characterSprites[1];
                cIsPlaying = false;

            }
            if (eventStoryNumber == 5)
            {
                cIsPlaying = true;
                

                StartCoroutine(AnimateText());

                dialogChar.sprite = null;
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 6)
            {
                bro1.SetActive(true);
                bro2.SetActive(true);
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[3];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 7)
            {
                cIsPlaying = true;

                StartCoroutine(AnimateText());

                dialogChar.sprite = characterSprites[4];
                //insère anim si y a pour le style
                cIsPlaying = false;
            }
            if (eventStoryNumber == 8)
            {
                foreach(GameObject bruh in renforts)
                {
                    bruh.SetActive(false);
                }
                bro1.SetActive(false);
                bro2.SetActive(false);
                gm.PrepareBattle(3);
                eventNumber++;
                eventStoryNumber = 1;
            }
        }

        yield return null;
    }

    
    //Coroutine écriture texte, vitesse de tape affecté par le WaitForSeconds
    IEnumerator AnimateText()
    {
        textIsComplete = false;
        for (int i = 0; i < (goatText[currentlyDisplayingText].Length + 1); i++)
        {
            dialog.text = goatText[currentlyDisplayingText].Substring(0, i);
            yield return new WaitForSeconds(displaySpeed);
        }
        displaySpeed = 0.08f;
        textIsComplete = true;
    }
}
