﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealVisor : TileIdentity
{
    Player player;
    bool spir = true;
    int damage=0;
    float paraTime=0;
    BoxCollider2D box;
    SpriteRenderer sr;
    Color a= new Color(0,0,0,0);
    Color b = new Color(0,0,0,1);
    float lerped=0;
    bool sensCol=true;
    bool Awaken = false;
    bool canHit = true;

    public override void Starting()
    {
        player = FindObjectOfType<Player>();
        box = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
        sr.color = a;
        box.enabled=false;
        Awaken = true;
    }

    private void Update()
    {
        if (Awaken)
        {
            if (sr.color.a < 1 && sensCol)
            {
                lerped += Time.deltaTime * 5;
                sr.color = Color.Lerp(a, b, lerped);
            }
            else if (sr.color.a > 0 && !sensCol)
            {
                lerped -= Time.deltaTime * 5;
                sr.color = Color.Lerp(a, b, lerped);
            }

            if (sr.color.a >= 1 && sensCol)
            {
                sensCol = false;
            }
            if (sr.color.a <= 0 && !sensCol)
            {
                sensCol = true;
                Move();
            }
        }
    }


    public void Move()
    {
        if (spir)
        {
            if (posX < gm.maxSens0-1 && posY == 3)
            {
                transform.Translate(new Vector3(2.17f, 0, 0));
                posX++;
            }
            else if (posX == gm.maxSens0-1 && posY > 0)
            {
                transform.Translate(new Vector3(0, -1.1f, 0));
                posY--;
            }
            else if (posX > 0 && posY == 0)
            {
                transform.Translate(new Vector3(-2.17f, 0, 0));
                posX--;
            }
            else if (posX == 0 && posY <2)
            {
                transform.Translate(new Vector3(0, 1.1f, 0));
                posY++;
            }
            else if(posX<gm.maxSens0-2 && posY==2)
            {
                transform.Translate(new Vector3(2.17f, 0, 0));
                posX++;
            }
            else if(posX== gm.maxSens0 - 2 && posY == 2)
            {
                transform.Translate(new Vector3(0, -1.1f, 0));
                posY--;
            }
            else if(posX>1 && posY == 1)
            {
                transform.Translate(new Vector3(-2.17f, 0, 0));
                posX--;
                
            }
            else if(posX==1 && posY == 1)
            {
                spir = false;
            }
        }
        else
        {
            if (posX !=0 && posX<gm.maxSens0-2  && posY == 1)
            {
                transform.Translate(new Vector3(2.17f, 0, 0));
                posX++;
            }
            else if (posX == gm.maxSens0 -2  && posY == 1)
            {
                transform.Translate(new Vector3(0, 1.1f, 0));
                posY++;
            }
            else if (posX >0 && posX!= gm.maxSens0-1 && posY == 2)
            {
                transform.Translate(new Vector3(-2.17f, 0, 0));
                posX--;
            }
            else if(posX==0 && posY>0 && posY != 3)
            {
                transform.Translate(new Vector3(0, -1.1f, 0));
                posY--;
            }
            else if(posX< gm.maxSens0-1 && posY == 0)
            {
                transform.Translate(new Vector3(2.17f, 0, 0));
                posX++;
            }
            else if(posX== gm.maxSens0-1 && posY < 3)
            {
                transform.Translate(new Vector3(0, 1.1f, 0));
                posY++;
            }
            else if(posX>0 && posY == 3)
            {
                transform.Translate(new Vector3(-2.17f, 0, 0));
                posX--;
            }
            else if(posX==0 && posY == 3)
            {
                spir = true;
            }
        }
        canHit = true;
    }

    private void FixedUpdate()
    {
        if(player.posX ==posX && player.posY==posY && sr.color.a >= 0.5f && canHit)
        {
            player.Damaged(paraTime, damage);
            player.RemoveCard();
            canHit = false;
        }
    }

}
