﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotgunScroll : Scrolls
{
    public Player playerScript;
    int dakkad;
    float stunning;
    int chargeLeft;
    public GameObject shotgunning;
    public GameManager gm;
    bool ok = false;


    public override void Starting()
    {
        playerScript = FindObjectOfType<Player>();
        gm = FindObjectOfType<GameManager>();
    }

    public override void Casting(int dakka, float stun)
    {
        chargeLeft = 3;
        playerScript.ammo = chargeLeft;
        dakkad = dakka;
        stunning = stun;
        playerScript.charged = this;
        GetComponent<AudioSource>().Play();
    }

    public override void Equipped()
    {
        playerScript.attacking = true;
        playerScript.stopped = true;
        playerScript.anim.SetTrigger("Attacking");
        
        playerScript.anim.speed = 3;
        ok = true;

        chargeLeft--;
        playerScript.ammo = chargeLeft;
        if (chargeLeft <= 0)
        {
            playerScript.charged = null;
        }
    }

    private void Update()
    {
        if(playerScript.sr.sprite==playerScript.attackSprite && ok)
        {
            playerScript.attacking = false;
            ok = false;
            Vector3 potato = new Vector3(player.transform.position.x + 2.15f, player.transform.position.y, player.transform.position.z);
            GameObject temp = Instantiate(shotgunning, potato, Quaternion.identity);
            temp.GetComponent<Projectile>().SetStats(playerScript.posX + 1, playerScript.posY, 1, gm);
            temp.GetComponent<Projectile>().MoreDakka(dakkad, stunning);
        }
        if (playerScript.myState == PlayerState.Stun)
        {
            StopAllCoroutines();
            playerScript.stopped = false;
        }
    }

}
