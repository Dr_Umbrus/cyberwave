﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaStealVFX : MonoBehaviour
{
    public AreaStealProj asp;
    public int actualSprite;
    public GameObject next;
    SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (asp.animationed[actualSprite] != asp.lastSprite)
        {
            actualSprite++;
            sr.sprite = asp.animationed[actualSprite];
        }
        else
        {
            if (next != null)
            {
                
                Destroy(gameObject);
            }
            else
            {
                asp.Steal();
            }
            
        }

        if (actualSprite == 6)
        {
            if(next!=null)
            next.SetActive(true);
        }
    }

}
