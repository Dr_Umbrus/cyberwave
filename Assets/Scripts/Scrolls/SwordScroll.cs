﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordScroll : Scrolls
{
    public GameObject summonSword;
    public Player playerScript;

    public override void Starting()
    {
        playerScript = FindObjectOfType<Player>();
    }
    public override void Casting(int dakka, float stun)
    {
        StartCoroutine(WaitShot(dakka, stun));
    }

    private void Update()
    {
        if (playerScript.myState == PlayerState.Stun)
        {
            StopAllCoroutines();
            playerScript.stopped = false;
        }
    }

    IEnumerator WaitShot(int dakka, float stun)
    {
        playerScript.stopped = true;
        yield return new WaitForSeconds(0.1f);
        Vector3 potato = new Vector3(player.transform.position.x + 2.5f, player.transform.position.y, player.transform.position.z);
        GameObject temp = Instantiate(summonSword, potato, Quaternion.identity);
        temp.GetComponent<Projectile>().MoreDakka(dakka, stun);
        yield return new WaitForSeconds(0.15f);
        playerScript.stopped = false;
    }

}
