﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MettaurWave : Projectile
{
    
    public Color startCol, endCol;
    SpriteRenderer sr;
    BoxCollider2D col;
    public Sprite move, hit, colStop;
    bool moved = false;
    AudioSource audioS;
    public GameObject player;
    public Player tile;
    public Vector2 offset;



    public override void Started()
    {
        sr = GetComponent<SpriteRenderer>();
        col = GetComponent<BoxCollider2D>();
        audioS =GetComponent<AudioSource>();
        offset = col.offset;
        tile = FindObjectOfType<Player>();
    }

    public void Update()
    {
        if (col.offset != offset)
        {
            col.offset = offset;
        }

        if (sr.sprite == move && !moved)
        {
            moved = true;
            
            
            posX--;
            if (posX < 0)
            {
                Destroy(gameObject);
            }
            else
            {
                
                transform.Translate(new Vector3(-2.1f, 0, 0));
            }
        }

        if (sr.sprite == colStop)
        {
            col.enabled = false;
            gm.CheckPos(posX, posY).GetComponent<SpriteRenderer>().color = Color.white;
        }

        if (sr.sprite == hit)
        {
            audioS.Play();
            moved = false;
            col.enabled = true;
            gm.CheckPos(posX, posY).GetComponent<SpriteRenderer>().color = Color.yellow;
            col.offset = new Vector2(100, 100);
        }

        
        if(player==null && posX==tile.posX && posY == tile.posY && col.enabled)
        {
            player = tile.gameObject;
            tile.Damaged(paraTime, damage);
        }

        
    }

    
}
