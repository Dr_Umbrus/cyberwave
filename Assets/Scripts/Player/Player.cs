﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum PlayerState { Normal, Def, Charged, Stun, Attacking}
public class Player : TileIdentity
{

    public PlayerState myState=PlayerState.Normal;
    public ScrollManager sm;
    bool vMove = true, hMove = true;
    public float chargeTime, maxChargeTime;
    public GameObject chargeShot;
    public Scrolls charged;
    float paraTime;
    public Color startColor, chargedColor, stunColor, defColor;
    public SpriteRenderer sr;
    public bool defense = false;
    public float defTime, maxDefTime, defDur, maxDefDur;
    public GameObject shield;
    public Text life;
    public Image[] hand;
    public Image[] handSquares;
    public Slider chargingShot;
    public Image chargingShotColor;
    public GameObject chargeVisual;
    public int ammo;
    public Text showWeapon;
    public AudioSource audios;
    public AudioClip damaged, chargeded;
    public Animator anim;
    public Sprite attackSprite;
    public Sprite baseSprite;
    public bool attacking;
    float countDown;

    public List<int> cards;

    public bool stopped = false;

    public int dakka = 0;
    public float stun = 0;

    bool sounded=false, soundeded = false;

    public override void Starting()
    {
        audios = GetComponent<AudioSource>();
        gm = FindObjectOfType<GameManager>();
        sm = FindObjectOfType<ScrollManager>();
        chargeTime = maxChargeTime;
        life.text = gm.life.ToString();
        chargingShot.maxValue = maxDefTime;
        defTime = maxDefTime;
    }

    private void Update()
    {
        if(sr.sprite==baseSprite && !attacking)
        {
            stopped = false;
        }
        if (countDown > 0)
        {
            countDown -= Time.deltaTime;
        }

        if (paraTime > 0)
        {
            paraTime -= Time.deltaTime;
        }
        else if (paraTime <= 0 && (myState == PlayerState.Stun || (!stopped && myState!=PlayerState.Normal)))
        {
            myState = PlayerState.Normal;
        }


        if (defTime > 0)
        {
            defTime -= Time.deltaTime;

        }
        else if (defTime <= 0 && myState == PlayerState.Normal)
        {
            defTime = 0;
            if (!sounded)
            {
                sounded = true;
                audios.clip = chargeded;
                audios.Play();
            }
            
        }

        if (defDur > 0)
        {
            sounded = true;
            defDur -= Time.deltaTime;
            if(myState != PlayerState.Stun)
            {
                myState = PlayerState.Def;
            }
        }
        else if(defense)
        {
            
            defTime = maxDefTime;
            defense = false;
            shield.SetActive(false);
            if(myState == PlayerState.Def)
            {
                myState = PlayerState.Normal;
            }
        }
        
        if (chargeTime > 0)
        {
            chargeTime -= Time.deltaTime;
            chargeVisual.SetActive(true);
        }
        else if (chargeTime <= 0 && myState == PlayerState.Normal)
        {
            myState = PlayerState.Charged;
            chargeVisual.SetActive(false);
            if (!soundeded)
            {
                soundeded = true;
                audios.clip = chargeded;
                audios.Play();
            }
        }

        chargingShot.value = chargingShot.maxValue - defTime;
        if(chargingShot.value == chargingShot.maxValue)
        {
            chargingShotColor.color = Color.green;
        }
        else
        {
            chargingShotColor.color = Color.blue;
        }

        if (charged != null)
        {
            showWeapon.text = charged.name + " (" + ammo + ")";
        }
        else
        {
            showWeapon.text = "Basic Shot";
        }

        if (Time.timeScale != 0 && myState != PlayerState.Stun && !stopped && myState!=PlayerState.Def)
        {
            if (defTime <= 0 && Input.GetAxis("Defend")!=0)
            {
                shield.SetActive(true);
                defDur = maxDefDur;
                defense = true;
            }

            if(Input.GetButtonDown("Cast") && cards.Count > 0 && countDown<=0)
            {
                UseCard();
            }

            if (Input.GetButtonDown("Shoot") && chargeTime <= 0)
            {
                soundeded = false;
                if (charged == null)
                {
                    GameObject temp = Instantiate(chargeShot, transform.position, Quaternion.identity);
                    temp.GetComponent<BasicChargeShot>().SetStats(posX, posY, 1, gm);
                    chargeTime = maxChargeTime;
                    myState = PlayerState.Normal;

                }
                else
                {
                    charged.Equipped();
                    chargeTime = maxChargeTime;
                    myState = PlayerState.Normal;
                }
            }


            if ((Input.GetButtonDown("Left") || Input.GetAxis("Hax") < -0.3f) && hMove)
            {
                hMove = false;
                canMove = gm.CheckMove(posX, posY, side, -1, 0);
                if (canMove)
                {
                    posX--;
                    transform.Translate(new Vector3(-2.15f, 0, 0));
                }
            }
            if ((Input.GetButtonDown("Right") || Input.GetAxis("Hax") >0.3f) && hMove)
            {
                hMove = false;
                canMove = gm.CheckMove(posX, posY, side, 1, 0);
                if (canMove)
                {
                    posX++;
                    transform.Translate(new Vector3(2.15f, 0, 0));
                }
            }
            if ((Input.GetButtonDown("Down") || Input.GetAxis("Vax") >0.3f) && vMove)
            {
                vMove = false;
                canMove = gm.CheckMove(posX, posY, side, -1, 1);
                if (canMove)
                {
                    posY--;
                    transform.Translate(new Vector3(0, -1.1f, 0));
                }
            }
            if ((Input.GetButtonDown("Up") || Input.GetAxis("Vax") <-0.3f) && vMove)
            {
                vMove = false;
                canMove = gm.CheckMove(posX, posY, side, 1, 1);
                if (canMove)
                {
                    posY++;
                    transform.Translate(new Vector3(0, 1.1f, 0));
                }
                
            }

            if (Input.GetAxis("Hax") > -0.3f && Input.GetAxis("Hax") < 0.3f)
            {
                hMove = true;
            }
            if (Input.GetAxis("Vax") > -0.3f && Input.GetAxis("Vax") < 0.3f)
            {
                vMove = true;
            }
        }

        if (stopped)
        {
            myState = PlayerState.Attacking;
        }

        switch (myState)
        {
            case PlayerState.Normal:
                sr.color = startColor;
                break;
            case PlayerState.Def:
                sr.color = defColor;
                break;
            case PlayerState.Charged:
                    sr.color = chargedColor;
                
                break;
            case PlayerState.Stun:
                sr.color = stunColor;
                break;

        }
        if (posY == 3)
        {
            life.color = Color.white;
            
        }
        else
        {
            life.color = Color.black;
        }
    }

    void UseCard()
    {
        Buff();
        if (cards.Count > 0) { 

            sm.Cast(sm.deck[cards[0]].spell, dakka, stun);
            cards.RemoveAt(0);
            dakka = 0;
            stun = 0;
        }

        int indexed=0;
        for(int i=0; i<cards.Count; i++)
        {
            if (!sm.buff[sm.deck[cards[i]].spell])
            {
                hand[indexed].sprite = sm.icones[sm.deck[cards[i]].spell];
                hand[indexed].color = Color.white;
                handSquares[indexed].color = Color.white;
                indexed++;
            }
        }
        if (indexed < 7)
        {
            for (int i = indexed; i < hand.Length; i++)
            {
                hand[i].sprite = null;
                hand[i].color = Color.clear;
                handSquares[i].color = Color.clear;
            }
        }
        countDown = 0.5f;
    }

    void Buff()
    {
        List<int> casted = new List<int>();
        bool keep = true;

        for(int i=1; i<cards.Count; i++)
        {
            if(sm.buff[sm.deck[cards[i]].spell] && keep)
            {
                casted.Add(i);
            }

            if (!sm.buff[sm.deck[cards[i]].spell])
            {
                keep = false;
            }
        }
        if (casted.Count > 0)
        {
            for (int i = 0; i < casted.Count; i++)
            {
                sm.Cast(sm.deck[cards[casted[i]]].spell, dakka, stun);
            }
            for (int i = casted.Count - 1; i >= 0; i--)
            {
                cards.RemoveAt(casted[i]);
            }
        }
        
    }

    public void Damaged(float paralyse, int damage)
    {
        if (defDur <= 0 || damage<0)
        {
            if (damage > 10)
            {
                audios.clip = damaged;
                audios.Play();
            }


            if (paraTime <= 0)
            {
                paraTime = paralyse;
                myState = PlayerState.Stun;
            }
            
            gm.life -= damage;
            if (gm.life <= 0)
            {
                Destroy(gameObject);
            }
        }
        if (gm.life > gm.maxLife)
        {
            gm.life = gm.maxLife;
        }
        life.text = gm.life.ToString();
    }

    public void RemoveCard()
    {
        if (cards.Count > 0)
        {
            cards.RemoveAt(0);
        }

        int indexed = 0;
        for (int i = 0; i < cards.Count; i++)
        {
            if (!sm.buff[sm.deck[cards[i]].spell])
            {
                hand[indexed].sprite = sm.icones[sm.deck[cards[i]].spell];
                hand[indexed].color = Color.white;
                handSquares[indexed].color = Color.white;
                indexed++;
            }
        }
        if (indexed < 7)
        {
            for (int i = indexed; i < hand.Length; i++)
            {
                hand[i].sprite = null;
                hand[i].color = Color.clear;
                handSquares[i].color = Color.clear;
            }
        }
    }

}
