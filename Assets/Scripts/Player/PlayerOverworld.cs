﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOverworld : MonoBehaviour
{
    public GameManager gm;
    public int speed;
    public StoryEvent scriptEvent;
    public bool stopped;
    
    void Awake()
    {
        scriptEvent = FindObjectOfType<StoryEvent>();
        gm = FindObjectOfType<GameManager>();
         gm.played = this;
        Time.timeScale = 1;
    }

    
    void FixedUpdate()
    {
        if (!stopped)
        {
            transform.Translate(new Vector2(Input.GetAxis("HorizontalC"), Input.GetAxis("Vertical")) * speed * Time.deltaTime);
        }
        
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            gm.PrepareBattle(1);
        }
        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            gm.PrepareBattle(3);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "InitialEventTrigger")
        {
            Destroy(collision.gameObject, 0);
            scriptEvent.InitiateStory();
        }
    }
}
