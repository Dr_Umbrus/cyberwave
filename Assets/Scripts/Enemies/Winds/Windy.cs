﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Windy : Projectile
{
    public int speed;

    public override void Pattern()
    {
        transform.Translate(new Vector3(speed * sens, 0, 0) * Time.deltaTime);
        
    }
}
