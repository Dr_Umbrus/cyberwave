﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomber : Enemies
{
    public GameObject bombin;
    public bool a, b, c, d;

    public override void Pattern()
    {

        if (timeAttack <= 0)
        {
            if (timeAttack <= 0)
            {
                stopped = true;
                attacking = true;
                timeAttack = Random.Range(1, 4);
                anim.SetTrigger("Attacking");


            }
        }

        if (sr.sprite == attackSprite && attacking)
        {
            attacking = false;
            GameObject temp = Instantiate(bombin, new Vector3(transform.position.x - 4 * 2.15f, transform.position.y - offset, transform.position.z), Quaternion.identity);
            temp.GetComponent<Projectile>().SetStats(posX - 4, posY, 0, gm);
        }
        if (!stopped)
        {
            if (timeMove > 0)
            {
                timeMove -= Time.deltaTime;
            }
            else
            {
                int move = -1;
                a = b = c = false;

                while (!canMove)
                {
                    if (a && b && c && d)
                    {
                        move = -1;
                        canMove = true; ;
                    }
                    else
                    {
                        move = Random.Range(0, 4);
                    }

                    if (move == 0)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, -1, 0);
                        if (!canMove)
                        {
                            a = true;
                        }
                    }
                    if (move == 1)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, 1, 0);
                        if (!canMove)
                        {
                            b = true;
                        }
                    }
                    if (move == 2)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, 1, 1);
                        if (!canMove)
                        {
                            c = true;
                        }
                    }
                    if (move == 3)
                    {
                        canMove = gm.CheckMove(posX, posY, 1, -1, 1);
                        if (!canMove)
                        {
                            d = true;
                        }
                    }
                }
                if (move == 0)
                {
                    posX--;
                    transform.Translate(new Vector3(-2.15f, 0, 0));
                }
                if (move == 1)
                {
                    posX++;
                    transform.Translate(new Vector3(2.15f, 0, 0));
                }
                if (move == 2)
                {
                    posY++;
                    transform.Translate(new Vector3(0, 1.1f, 0));
                }
                if (move == 3)
                {
                    posY--;
                    transform.Translate(new Vector3(0, -1.1f, 0));
                }

                canMove = false;

                timeMove = startTimeMove;
                timeAttack--;

            }
        }
    }
}
