﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberBomb : Projectile
{
    public float timeToBomb;
    public float timeToShine;
    public float maxTimeToShine;
    public Color startCol, clignoteCol;
    public int speed;
    Vector3 targetPos;
    public GameObject explosion;
    public List<SpriteRenderer> targetCases;

    private void Start()
    {
        targetPos = transform.position;
        transform.Translate(new Vector3(0, 5, 0));
        for(int i=0; i<gm.battleSol.Count; i++)
        {
            if ((gm.battleSol[i].posX == posX + 1 && gm.battleSol[i].posY == posY) || (gm.battleSol[i].posX == posX - 1 && gm.battleSol[i].posY == posY) || (gm.battleSol[i].posX == posX && gm.battleSol[i].posY == posY) || (gm.battleSol[i].posX == posX && gm.battleSol[i].posY == posY + 1) || (gm.battleSol[i].posX == posX && gm.battleSol[i].posY == posY - 1))
            {
                targetCases.Add(gm.battleSol[i].GetComponent<SpriteRenderer>());
            }
        }

    }

    public override void Pattern()
    {
        if (timeToBomb >= 0)
        {
            timeToBomb -= Time.deltaTime;
            Clignote();
        }
        else
        {
            transform.Translate(new Vector3(0, -speed, 0) * Time.deltaTime);
            if (transform.position.y <= targetPos.y)
            {
                Vector3 side = new Vector3(0.15f, 0, 0);

                Instantiate(explosion, targetPos+side, Quaternion.identity);
                

                foreach(SpriteRenderer cases in targetCases)
                {
                        if (cases.color == clignoteCol)
                        {
                            cases.color = startCol;
                        }
                    }
                    Destroy(gameObject);
                }
            }
        }

        void Clignote()
        {
            if (timeToShine >= 0)
            {
                timeToShine -= Time.deltaTime;

            }
            else
            {
                timeToShine = maxTimeToShine;
            foreach (SpriteRenderer cases in targetCases)
            {
                
                        if (cases.color == clignoteCol)
                        {
                    cases.color = startCol;
                        }
                        else
                        {
                    cases.color = clignoteCol;
                        }
                    }
            }
        }
}
