﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MookMaker : Enemies
{
    public GameObject mook;
    public Grid grille;

    public override void Starting2()
    {
        startTimeAttack += Random.Range(-10, 11) / 10;
        timeAttack = startTimeAttack;
        grille = FindObjectOfType<Grid>();
    }
    public override void Pattern()
    {

        

        if (timeAttack > 0 && !stopped)
        {
            timeAttack -= Time.deltaTime;
        }

        if (timeAttack <= 0)
        {
            timeAttack = startTimeAttack;
            anim.SetTrigger("Attacking");
            stopped = true;
            attacking = true;
        }

        if (sr.sprite == attackSprite && attacking)
        {
            int x = -1;
            int posA=0;
            int posB=0;
            bool check = false;
            while (!check)
            {
                x = Random.Range(0, 16);
                posA = gm.battleSol[x + gm.battleSol.Count / 2].posX;
                posB = gm.battleSol[x + gm.battleSol.Count / 2].posY;
                check = gm.CheckMove(posA, posB, 1, -1, 0);
                
            }
            x += 16;
            GameObject temp = Instantiate(mook, new Vector3(gm.battleSol[x].transform.position.x, gm.battleSol[x].transform.position.y + mook.GetComponent<TileIdentity>().offset, gm.battleSol[x].transform.position.z - 0.5f), Quaternion.identity, grille.transform);
            temp.GetComponent<TileIdentity>().posX = posA;
            temp.GetComponent<TileIdentity>().posY = posB;
            attacking = false;
        }


    }
}
