﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSwordScroll : Scrolls
{
    public GameObject summonSword;
    public Player playerScript;
    public override void Casting(int dakka, float stun)
    {
        StartCoroutine(DashSword(dakka, stun));
    }

    IEnumerator DashSword(int dakka, float stun)
    {
        playerScript.stopped = true;
        playerScript.transform.Translate(2.17f * 2, 0, 0);
        yield return new WaitForSeconds(0.15f);
        Vector3 potato = new Vector3(player.transform.position.x + 2.5f, player.transform.position.y, player.transform.position.z);
        GameObject temp = Instantiate(summonSword, potato, Quaternion.identity);
        temp.GetComponent<Projectile>().MoreDakka(dakka, stun);
        yield return new WaitForSeconds(0.15f);
        playerScript.transform.Translate(2.17f * 2, 0, 0);
        yield return new WaitForSeconds(0.1f);
        playerScript.stopped = false;
    }
}
