﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int posX, posY;
    public GameManager gm;
    public int damage;
    public int sens;
    public float paraTime;
    public float endX;


    public virtual void Started()
    {

    }

    public void SetStats(int posX2, int posY2, int sens2, GameManager gm2)
    {
        gm = gm2;
        posX = posX2;
        posY = posY2;
        sens = sens2;
        Started();
    }

    public void MoreDakka(int dakka, float dur)
    {
        damage += dakka;
        paraTime += dur;

    }



    // Update is called once per frame
    void Update()
    {
        Pattern();

        if((sens==-1 && transform.position.x<endX) || (sens==1 && transform.position.x > endX))
        {
            Destroy(gameObject);
        }
    }

    public virtual void Pattern()
    {

    }
}
