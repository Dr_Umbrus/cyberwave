﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionLob : MonoBehaviour
{
    public int posX, posY;

    public void SetStats(int x, int y)
    {
        posX = x;
        posY = y;
    }
}
