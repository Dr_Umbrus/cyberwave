﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSol : MonoBehaviour
{

    public Material blu, raid;
    void Start()
    {
        Coloring();
    }

    public void Coloring()
    {
        if (GetComponentInParent<TileIdentity>().side == 1)
        {
            GetComponent<SpriteRenderer>().material = raid;
        }
        else
        {
            GetComponent<SpriteRenderer>().material = blu;
        }
    }
}
