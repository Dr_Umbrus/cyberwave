﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Protector: Enemies
{
    public Enemies mook=null;
    public GameObject shieldPre, backPre;
    List<Enemies> bros= new List<Enemies>();
    public float timeShield;

    public override void Starting2()
    {
        startTimeAttack += Random.Range(-10, 11) / 10;
        timeAttack = startTimeAttack;
    }
    public override void Pattern()
    {

        

        if (timeAttack > 0 && !stopped)
        {
            timeAttack -= Time.deltaTime;
        }

        if (timeAttack <= 0)
        {
            timeAttack = startTimeAttack;
            anim.SetTrigger("Attacking");
            stopped = true;
            attacking = true;
        }

        if (sr.sprite == attackSprite && attacking)
        {
            StartCoroutine(Shielding());

            attacking = false;
        }


    }

    IEnumerator Shielding()
    {
        bros.Clear();

        for (int i = 0; i < gm.battleEntities.Count; i++)
        {
            if (gm.battleEntities[i].GetComponent<Enemies>() != null)
            {
                bros.Add(gm.battleEntities[i].GetComponent<Enemies>());
            }
        }

        var ally = -1;
        var ok = false;

        while (!ok)
        {
            ok = true;
            ally = Random.Range(0, bros.Count);
            if (bros[ally] == mook)
            {
                ok = false;
            }
        }

        mook = bros[ally];
        var shield = Random.Range(0, 2);
        GameObject temp;
        if (shield == 0)
        {
            mook.shielded = true;
            temp = Instantiate(shieldPre, transform.position, Quaternion.identity, mook.transform);
            temp.transform.localPosition = new Vector3(0, 0, -0.1f);
        }
        else
        {
            mook.backDamage = true;
            temp = Instantiate(backPre, transform.position, Quaternion.identity, mook.transform);
            temp.transform.localPosition = new Vector3(0, 0, -0.1f);
        }
        
        yield return new WaitForSeconds(timeShield);
        Destroy(temp);
        mook.shielded = false;
        mook.backDamage = false;

    }
}
