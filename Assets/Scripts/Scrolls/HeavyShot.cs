﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyShot : Projectile
{
    public float speed;
    public Animator an;
    bool dying = false;

    
    public override void Pattern()
    {
        if(!dying)
        transform.Translate(new Vector3(speed * sens, 0, 0) * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!dying)
        {
            if (collision.gameObject.tag == "enemy")
            {
                collision.gameObject.GetComponent<Enemies>().Damaged(damage, paraTime);
            }

            if (collision.gameObject.tag != "player")
            {
                StartCoroutine(Death());
            }
        }
    }

    IEnumerator Death()
    {
        an.SetBool("death", true);
        dying = true;
        yield return new WaitForSeconds(0.2f);
        Destroy(gameObject);
    }
}
