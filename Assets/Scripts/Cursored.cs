﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursored : MonoBehaviour
{
    public int posX, posY;
    bool hMove=true, vMove=true;
    public Vector3[] positions;

    public VisualManager vm;
    public ScrollManager sm;

    public int chosenLetter;
    public int chosenType;
    int calc;

    private void Awake()
    {
        posX = 0;
        posY = 0;
        transform.position = positions[0];
        chosenLetter = 0;
        chosenType = 0;

        for (int i = 0; i < vm.hand.Length; i++)
        {
            positions[i] = vm.hand[i].transform.localPosition;
        }
    }

    private void Update()
    {
        if ((Input.GetButtonDown("Left") || Input.GetAxis("Hax") < -0.3f) && hMove)
        {
            hMove = false;
            if (posX > 0)
            {
                posX--;
            }

            else
            {
                if(vm.hand[4 + 5 * posY].sprite != null)
                {
                    posX = 4;
                }
                else if(vm.hand[3 + 5 * posY].sprite != null)
                {
                    posX = 3;
                }
                else if(vm.hand[2 + 5 * posY].sprite != null)
                {
                    posX = 2;
                }
                else if (vm.hand[1 + 5 * posY].sprite != null)
                {
                    posX = 1;
                }
            }
               
        }
        if ((Input.GetButtonDown("Right") || Input.GetAxis("Hax") > 0.3f) && hMove)
        {
            hMove = false;
            if (vm.hand[posX + 1 + 5 * posY].sprite == null || posX == 4)
            {
                posX = 0;
            }
            else
            {
                posX++;
            }
        }
        if ((Input.GetButtonDown("Down") || Input.GetAxis("Vax") < -0.3f) && vMove)
        {
            vMove = false;
            if (posY == 1)
            {
                posY = 0;
            }
            else if (vm.hand[posX + 5].sprite != null)
            {
                posY = 1;
            }
        }
        if ((Input.GetButtonDown("Up") || Input.GetAxis("Vax") > 0.3f) && vMove)
        {
            vMove = false;
            if (posY == 1)
            {
                posY = 0;
            }
            else if (vm.hand[posX + 5].sprite != null)
            {
                posY = 1;
            }
        }

        if (Input.GetAxis("Hax") >-0.3f && Input.GetAxis("Hax") < 0.3f)
        {
            hMove = true;
        }
        if (Input.GetAxis("Vax") > -0.3f && Input.GetAxis("Vax") < 0.3f)
        {
            vMove = true;
        }

        calc = posX + 5 * posY;
        Positionner();

        if (Input.GetButtonDown("Cast"))
        {
            Clic();
        }
        if (Input.GetButtonDown("No"))
        {
            UnClic();
        }
        if (Input.GetButtonDown("Select"))
        {
            Back();
        }
    }

    void Back()
    {
        vm.BackToGame();
    }
    void Positionner()
    {
        transform.localPosition = positions[calc];
        vm.ChangeDisplay(calc);
    }

    void Clic()
    {
        if (vm.whichSelected[0] == -1)
        {
            chosenLetter = sm.deck[sm.cardsInHand[calc]].letter;
            chosenType = sm.deck[sm.cardsInHand[calc]].spell;
            vm.Select(calc);
        }

        else if(vm.selected[calc]== false && (sm.deck[sm.cardsInHand[calc]].letter== chosenLetter || sm.deck[sm.cardsInHand[calc]].spell== chosenType || sm.deck[sm.cardsInHand[calc]].letter == 10))
        {
            vm.Select(calc);
        }
    }

    void UnClic()
    {
        if(vm.whichSelected[0] != -1)
        {
            vm.UnSelect();
        }
        
    }
}
