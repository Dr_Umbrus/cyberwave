﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardStealer : Enemies
{
    public GameObject visor;

    public override void Starting2() { 

        GameObject temp = Instantiate(visor, gm.battleSol[0].transform.position, Quaternion.identity);
        temp.transform.Translate(new Vector3(0, 0, -1));

    }
}
