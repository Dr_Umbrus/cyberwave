﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BroCac : Bro
{
    int posXSave=0, posYSave=0;
    bool endAttacking = false;

    public GameObject projectile;

    public override void Starting2()
    {
        startTimeAttack += Random.Range(-10, 11) / 10;
        timeAttack = startTimeAttack;
    }
    public override void Pattern()
    {
        if(sr.sprite==baseSprite && !endAttacking)
        {
            AttackEnd();
        }
        if (!stopped)
        {

            if (timeMove > 0)
            {
                timeMove -= Time.deltaTime;
            }
            else
            {
                int x = 0;
                int y = 0;

                canMove = false;
                while (!canMove)
                {
                    x = Random.Range(0, gm.maxSens0);
                    y = Random.Range(0, 4);

                    canMove = gm.CheckMove(x, y, 0, 0, 2);
                    if (x == posX && y == posY)
                    {
                        canMove = false;
                    }
                }
                posX = x;
                posY = y;
                transform.position = gm.spawnPosL[4 * (x) + y];
                transform.Translate(new Vector3(0, offset, -5));
                timeMove = startTimeMove;

            }
        }

        if (timeAttack > 0 && !stopped)
        {
            timeAttack -= Time.deltaTime;
        }

        if (timeAttack <= 0)
        {
            timeAttack = startTimeAttack;
            anim.SetTrigger("Attacking");
            stopped = true;
            endAttacking = true;
            attacking = true;
            AttackStart();
        }

        if (sr.sprite == attackSprite && attacking)
        {
            Attack();
            
        }


    }

    

    void AttackStart()
    {
        posXSave = posX;
        posYSave = posY;
        var test0 = false;
        var test1 = false;

        for(int i=0; i<gm.enemiesLeft.Count; i++)
        {
            if(!test0 && !test1)
            {
                test0 = gm.CheckMove(gm.enemiesLeft[i].posX - 1, gm.enemiesLeft[i].posY, 0, 0, 0);
                test1 = gm.CheckMove(gm.enemiesLeft[i].posX - 1, gm.enemiesLeft[i].posY, 1, 0, 0);
                posX = gm.enemiesLeft[i].posX - 1;
                posY = gm.enemiesLeft[i].posY;
            }
            
        }
        for (int i = 0; i < gm.battleSol.Count; i++)
        {
            if (posX == gm.battleSol[i].posX && posY == gm.battleSol[i].posY)
            {
                transform.position = gm.battleSol[i].transform.position;
                transform.Translate(new Vector3(0, 0, -3));
            }
        }
    }

    void Attack()
    {
        Vector3 potato = new Vector3(transform.position.x + 2.5f, transform.position.y, transform.position.z);
        GameObject temp = Instantiate(projectile, potato, Quaternion.identity);
        attacking = false;

        
    }

    void AttackEnd()
    {
        posX = posXSave;
        posY = posYSave;

        for (int i = 0; i < gm.battleSol.Count; i++)
        {
            if (posX == gm.battleSol[i].posX && posY == gm.battleSol[i].posY)
            {
                transform.position = gm.battleSol[i].transform.position;
                transform.Translate(new Vector3(0, 0, -3));
            }
        }
        endAttacking = true;
    }

}
