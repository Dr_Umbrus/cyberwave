﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbingObject : MonoBehaviour
{
    public Transform launcher;
    public Transform target;
    public GameObject grenade;
    // Start is called before the first frame update
    void Start()
    {
        launcher = FindObjectOfType<Player>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Instantiate(grenade, launcher.position, Quaternion.identity);
            //StartCoroutine(Feuer(launcher,target));
        }
    }

    IEnumerator Feuer(Transform depart, Transform arrivee)
    {
        var currentPosDepart = depart.position;
        var currentPosArrivee = arrivee.position;
        var t = 0f;
        var timeToFly = 2f;
        Instantiate(grenade,depart.position, Quaternion.identity);
        grenade.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,15));
        while (t < 1)
        {
            t += Time.deltaTime / timeToFly;
            grenade.transform.position = Vector2.Lerp(currentPosDepart, currentPosArrivee, t);
            yield return null;
        }
        yield return null;
    }
}
