﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stun2 : Scrolls
{
    public Player playerScript = null;


    public override void Starting()
    {
        playerScript = FindObjectOfType<Player>();
    }

    public override void Casting(int dakka, float stun)
    {
        playerScript.stun += 2;
    }
}
