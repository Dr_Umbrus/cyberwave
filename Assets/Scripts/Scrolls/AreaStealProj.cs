﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaStealProj : MonoBehaviour
{
    public AreaSteal father;

    public Sprite[] animationed;
    public Sprite lastSprite;
    

    private void Update()
    {
        
    }

    public void Steal()
    {
        father.Stealing();
        Destroy(gameObject);
    }

    public void Father(AreaSteal potato)
    {
        father = potato;
    }
}
