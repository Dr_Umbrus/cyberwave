﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header ("Combat entities")] 
    public List<TileIdentity> battleTerrain;
    public List<TileIdentity> battleSol;
    public List<TileIdentity> battleEntities;
    public Player player;
    public PlayerOverworld played;

    [Header ("Positions")]
    public List<Vector3> spawnPosL;
    public List<Vector3> spawnPosR;

    [Header ("Spawn")]
    public Spawning spawn;
    public List<Enemies> enemiesLeft;

    [Header ("Combat interface")]
    public Slider combatLoad;
    public Image combatLoadColor;
    public GameObject cardMenu;

    [Header ("Stats")]
    public int life;
    public int maxLife;
    public int wave = 1;
    public int maxSens0;


    
    [Header ("Managers")]
    public ScrollManager sm;
    public VisualManager vm;
    public CombatManager cm;
    public StoryEvent se;
    
    [Header ("Sons")]
    AudioSource audioS;
    public AudioClip charged, retour;


    public GameObject combatNeed;

    public Vector3 lastPos;
    public Grid grille;

    int newBattle=-1;

    bool inCombat = false;

    void Start()
    {
        //grille = FindObjectOfType<Grid>();
        player = FindObjectOfType<Player>();
        audioS = GetComponent<AudioSource>();
        life = maxLife;
        combatNeed.SetActive(false);
        Time.timeScale = 1;
        var pot = battleTerrain.Count / 2;
        for (int i = 0; i < battleTerrain.Count/2; i++)
        {
            spawnPosL.Add(battleTerrain[i].transform.position);
            spawnPosR.Add(battleTerrain[i + pot].transform.position);
        }

    }

    public void PrepareBattle(int battle)
    {
        newBattle = battle;
        StartCoroutine(ToBattle());
        se.EndDial();
    }

    IEnumerator ToBattle()
    {

        inCombat = true;
        combatLoad.value = 0;

        SceneManager.LoadScene("SampleScene");

        lastPos = played.transform.position;

        yield return 0;

        combatNeed.SetActive(true);

        while (spawn == null)
        {
            yield return 0;
        }
        cm.SetSpawner(spawn, newBattle);
        sm.SummonScrolls();
        StartBattle();
    }

    

    public void EndCombat()
    {
        combatLoad.value = 0;
        inCombat = false;
        spawn = null;
        Time.timeScale = 1;
        StartCoroutine(BackToMap());
        
    }

    IEnumerator BackToMap()
    {
        combatNeed.SetActive(false);
        SceneManager.LoadScene("OW");
        yield return 0;
        FindObjectOfType<Camera>().farClipPlane = 0;
        yield return 0;
        played.transform.position = lastPos;
        FindObjectOfType<Camera>().farClipPlane = 1000;
        yield return 0;
        if(newBattle==1 || newBattle == 2)
        {
            se.InitiateStory();
        }
    }


    public void StartBattle()
    {
        
        maxSens0 = 4;
        
        cardMenu.SetActive(false);

        for (int i = 0; i < battleSol.Count; i++)
        {
            if (battleSol[i].side == 0)
            {
                spawnPosL.Add(battleSol[i].transform.position);
            }
            else
            {
                spawnPosR.Add(battleSol[i].transform.position);
            }
        }
        spawn.Spawner();
        spawn.Summon();
        //MyTurn();
    }

    public void UpdateEnemies()
    {
        if (enemiesLeft.Count == 0)
        {
            EndCombat();
        }
    }

    /*IEnumerator Respawn()
    {
        maxSens0 = 4;
        sm.Newwave();
        player.stopped = false;
        yield return new WaitForSeconds(0.5f);
        spawn[wave].Spawner();
        spawn[wave].Summon();
        for (int i = 0; i < battleSol.Count; i++)
        {
            if (battleSol[i].posX >= maxSens0)
            {
                battleSol[i].side = 1;
                battleSol[i].GetComponentInChildren<ColorSol>().Coloring();
            }
        }
        yield return new WaitForSeconds(1f);
        MyTurn();
        if (spawn.Length > wave + 1)
        {
            wave++;
        }
        spawning = false;
    }*/

    void Update()
    {
        if(inCombat)
        {
            combatLoad.value += Time.deltaTime;

            if (combatLoad.value >= combatLoad.maxValue)
            {
                if (combatLoadColor.color != Color.green)
                {
                    combatLoadColor.color = Color.green;
                    audioS.clip = charged;
                    audioS.Play();
                }

            }
        }
        

        if (combatLoad.value >= combatLoad.maxValue && Input.GetButtonDown("Select"))
        {
            MyTurn();
        }

        if (!inCombat)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                if (vm.deck == 0)
                {
                    vm.OpenDeck();
                    played.stopped = true;
                }
                else
                {
                    if (sm.deck.Count == 28)
                    {
                        vm.CloseDeck();
                        played.stopped = false;
                    }
                    
                }
            }
        }
    } 

    public void MyTurn()
    {
        combatLoad.value = 0;
        combatLoadColor.color = Color.red;
        Time.timeScale = 0;
        cardMenu.SetActive(true);
        vm.Clearing();
    }

    public void BackCombat()
    {
        cardMenu.SetActive(false);
        Time.timeScale = 1;
        audioS.clip = retour;
        audioS.Play();
    }
    

    public GameObject CheckPos(int posX, int posY)
    {
        GameObject thingy = null;
        foreach(TileIdentity thing in battleTerrain)
        {
            if(thing.posX==posX && thing.posY==posY && thing.tile <= 0)
            {
                thingy = thing.gameObject;
            }
        }

        return thingy;
    }

    public int[] CheckGrid(Vector3 pos)
    {
        int[] positions = new int[2];
        
        foreach (TileIdentity sol in battleSol)
        {
            Vector3 potatoto = sol.grid.LocalToCell(sol.transform.position);
            float potato = Mathf.Abs(Vector3.Distance(potatoto, pos));
            if (potato<1.2f)
            {
                
                positions[0] = sol.posX;
                positions[1] = sol.posY;
            }
        }
        return positions;
    }

    public bool CheckMove(int posX, int posY, int side, int sens, int direction)
    {
        bool test = true;

        if (direction==0)
        {
            if(posX+sens>=0 && posX + sens <= 7)
            {
                foreach (TileIdentity thing in battleTerrain)
                {
                    if (thing.posX == posX + sens && thing.posY == posY)
                    {
                        if ((thing.tile >=1 && thing.side != side) || thing.destroyed || thing.tile == 0)
                        {
                            test = false;
                        }
                    }
                }
            }
            else
            {
                test = false;
            }
        }
        else if (direction == 1)
        {
            if (posY + sens >= 0 && posY + sens <= 3)
            {
                foreach (TileIdentity thing in battleTerrain)
                {
                    if (thing.posY == posY + sens && thing.posX == posX)
                    {
                        if ((thing.tile >= 1 && thing.side != side) || thing.destroyed || thing.tile == 0)
                        {
                            test = false;
                            
                        }
                    }
                }
            }
            else
            {
                test = false;
                
            }
        }

        else
        {
            foreach (TileIdentity thing in battleTerrain)
            {
                if (thing.posY == posY && thing.posX == posX)
                {
                    if ((thing.tile <= 0 && thing.side != side) || thing.destroyed || thing.tile == 1)
                    {
                        test = false;
                        
                    }
                }
            }
        }
        return test;
    }
}
