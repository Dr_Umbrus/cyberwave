﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberExplosion : Projectile
{
    private void Start()
    {
        Destroy(gameObject, 1);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            collision.gameObject.GetComponent<Player>().Damaged(paraTime, damage);
        }
    }
}
