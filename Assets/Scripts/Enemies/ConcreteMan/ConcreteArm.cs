﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConcreteArm : MonoBehaviour
{
    public GameManager gm;
    public int posX, posY;
    public List<SpriteRenderer> targetCases;
    public List<TileIdentity> targetIdentity;
    bool moving = false;
    BoxCollider2D box;
    public int speed;
    public int damage;
    public float stun;
    public float targetY, offset;



    public void Stats(int x, int y, GameManager game, float tY)
    {
        transform.Translate(new Vector3(0, offset, 0));
        box = GetComponent<BoxCollider2D>();
        box.enabled = false;
        gm = game;
        posX = x;
        posY = y;
        targetY = tY;

        for (int i = 0; i < gm.battleSol.Count; i++)
        {

            if (gm.battleSol[i].posX >=posX && gm.battleSol[i].posY == posY && gm.battleSol[i].side==0)
            {
                targetCases.Add(gm.battleSol[i].GetComponent<SpriteRenderer>());
                targetIdentity.Add(gm.battleSol[i]);
            }
        }

        foreach (SpriteRenderer cases in targetCases)
        {
            cases.color = Color.red;
        }
    }

    private void Update()
    {
        if (moving)
        {
            transform.Translate(new Vector3(speed, 0, 0) * Time.deltaTime);
            for(int i=0; i<targetIdentity.Count; i++)
            {
                if(!targetIdentity[i].destroyed && transform.position.x > targetIdentity[i].transform.position.x)
                {
                    targetIdentity[i].Destroying();
                    targetCases[i].color = Color.white;
                }
            }

            if (transform.position.x > 10)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            transform.Translate(new Vector3(0, -speed, 0) * Time.deltaTime);
            if (transform.position.y <= targetY)
            {
                transform.position = new Vector3(transform.position.x, targetY, transform.position.z);
                moving = true;
                box.enabled = true;
                GetComponent<AudioSource>().Play();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Player>().Damaged(stun, damage);
        }
    }
}
