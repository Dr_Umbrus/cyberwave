﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shovel : Projectile
{

    AudioSource audioS;
    public GameObject player;
    public Vector2 offset;
    public float speed;
    float speedster = 1.5f;

    

    public override void Pattern()
    {
        transform.Translate(new Vector3(speed * sens, 0, 0) * Time.deltaTime);
        speedster += Time.deltaTime*5;
        if (speed < 50)
        {
            speed = speedster * speedster;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
            if (collision.gameObject.tag == "player" && player == null)
            {
                player = collision.gameObject;
                collision.gameObject.GetComponent<Player>().Damaged(paraTime, damage);
            }

    }
}
