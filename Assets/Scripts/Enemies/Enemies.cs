﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemies : TileIdentity
{
    public int life;
    public float timeMove, startTimeMove;
    public float timeAttack, startTimeAttack;
    public Player player;
    public float paraTime, maxParaTime;
    public Color baseColor, stunColor;
    public SpriteRenderer sr;
    public Text lifebar;
    public AudioSource audioS;
    public AudioClip damaged, death;
    public bool dying = false;
    public float timing;
    public float maxTiming;
    public BoxCollider2D box;

    public Animator anim;
    public Sprite attackSprite, baseSprite;
    public bool stopped;
    public bool attacking = false;

    public float summonTime=1.5f, startSummon=1.5f;
    public bool summoned = false;
    public bool shielded = false;

    public bool backDamage = false;
    


    public override void Starting()
    {
        summoned = false;
        summonTime = 1f;
        startSummon = 1f;
        box = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
        gm = FindObjectOfType<GameManager>();
        gm.enemiesLeft.Add(this);
        player = FindObjectOfType<Player>();
        lifebar.text = life.ToString();
        Starting2();
        audioS = gameObject.AddComponent<AudioSource>();
        maxTiming = death.length;
        baseSprite = sr.sprite;
        box.enabled = false;
    }
    public virtual void Starting2()
    {

    }

    void Update()
    {
            if (summonTime > 0 && !summoned)
            {
                summonTime -= Time.deltaTime;
                Color potato = sr.color;
                potato.a = 1 - Mathf.Lerp(0, 1, summonTime / startSummon);
                sr.color = potato;
            }
            else if (!summoned)
            {
                summoned = true;
                box.enabled = true;
            }
        

        if (summoned) { 
            if (paraTime > 0)
            {
                anim.speed = 0;
                paraTime -= Time.deltaTime;
            }
            else
            {
                anim.speed = 1;
                paraTime = 0;
            }
            if (sr.sprite == baseSprite && !attacking)
            {
                stopped = false;
            }
            if (!stopped)
            {
                
                if (sr.color != baseColor)
                {
                    if (maxParaTime > 0)
                    {
                        sr.color = Color.Lerp(baseColor, stunColor, paraTime / maxParaTime);
                    }
                    else
                    {
                        sr.color = baseColor;
                    }
                }

            }
            if (dying)
            {
                timing += Time.deltaTime;
                Color temp = sr.color;
                temp.a = Mathf.Lerp(0, 1, maxTiming - timing);
                sr.color = temp;
            }

            if (paraTime <= 0 && !dying)
            {
                Pattern();
            }
        }

            if (posY == 3)
            {
                lifebar.color = Color.white;

            }
            else
            {
                lifebar.color = Color.black;
            }
        
    }

    public virtual void Pattern()
    {

    }

    public void Damaged(int damage, float paralyse)
    {
        if (!shielded)
        {
            if (backDamage)
            {
                damage /= 2;
                player.Damaged(0, damage);
            }
            life -= damage;
            paraTime = paralyse;
            if (paralyse > 0)
            {
                maxParaTime = paraTime;
            }
            else
            {
                maxParaTime = 0.1f;
            }
            sr.color = stunColor;
            if (life <= 0)
            {
                StartCoroutine(Dying());

            }
            else
            {
                audioS.clip = damaged;
                audioS.Play();
            }
            if (life >= 0)
            {
                lifebar.text = life.ToString();
            }
            else
            {
                lifebar.text = "0";
            }
        }
    }

    IEnumerator Dying()
    {
        dying = true;
        gm.battleEntities.Remove(this);
        gm.battleTerrain.Remove(this);
        gm.enemiesLeft.Remove(this);
        audioS.clip = death;
        audioS.Play();
        GetComponent<Collider2D>().enabled=false;
        yield return new WaitForSeconds(death.length);
        gm.UpdateEnemies();
        Destroy(gameObject);
    }
}
