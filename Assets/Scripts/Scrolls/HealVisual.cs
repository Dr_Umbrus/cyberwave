﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealVisual : MonoBehaviour
{
    SpriteRenderer sr;
    public Sprite endSprite, healSprite;
    Player player;
    public int healValue;
    int actualHeal = 0;

    private void Start()
    {
        player = GetComponentInParent<Player>();
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (actualHeal<healValue)
        {
            player.Damaged(0, -2);
            actualHeal += 2;
            
        }
        if (sr.sprite == endSprite)
        {
            
            Destroy(gameObject);
        }
    }
} 
