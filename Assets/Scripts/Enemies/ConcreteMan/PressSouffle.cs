﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressSouffle : MonoBehaviour
{

    public int damage;
    public float stun;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Player>().Damaged(stun, damage);
        }
    }
}
