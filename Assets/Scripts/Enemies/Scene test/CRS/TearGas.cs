﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TearGas : ExplosionLob
{
    GameManager gm;
    Player player;
    public GameObject moreTears;
    public int wave=0;
    public int sens = -1;
    bool ok=false, launched=false;
    public float timer = 0;
    public float poisonTime, maxPoisonTime;
    SpriteRenderer sr;
    Color a = new Color(1, 1, 1, 0);
    Color b = new Color(1, 1, 1, 1);
    Bro[] bros;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        gm = FindObjectOfType<GameManager>();
        player = FindObjectOfType<Player>();
        bros = FindObjectsOfType<Bro>();
    }


    void Update()
    {
        var check = false;
        if(player.posX==posX && player.posY == posY && sr.color.a>0.5f)
        {
            check = true;
            if (poisonTime > 0)
            {
                poisonTime -= Time.deltaTime;
            }
            else
            {
                poisonTime = maxPoisonTime;
                player.Damaged(0, 1);
            }
            
        }
        for(int i=0; i<bros.Length; i++)
        {
            if (bros[i].posX == posX && bros[i].posY == posY && sr.color.a > 0.5f)
            {
                check = true;
                if (poisonTime > 0)
                {
                    poisonTime -= Time.deltaTime;
                }
                else
                {
                    poisonTime = maxPoisonTime;
                    bros[i].Damaged(0, 1);
                }

            }
        }

        if(!check)
        {
            poisonTime = maxPoisonTime;
        }

        
        if (!ok)
        {
            if (sr.color.a < 1)
            {
                timer += Time.deltaTime*2;
                sr.color = Color.Lerp(a, b, timer);
            }
            else if (!launched)
            {
                StartCoroutine(Dissipation());
                launched = true;    
            }
        }
        else
        {
            timer -= Time.deltaTime*1.5f;
            sr.color = Color.Lerp(a, b, timer);

            if (sr.color.a <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    public void Kiddo(int dir, int x, int y)
    {
        wave = 1;
        sens = dir;
        if (sens == 0)
        {
            posX = x-1;
            posY = y;
        }
        if (sens == 1)
        {
            posX = x;
            posY = y+1;
        }
        if (sens == 2)
        {
            posX = x+1;
            posY = y;
        }
        if (sens == 3)
        {
            posX = x;
            posY = y-1;
        }
    }

    IEnumerator Dissipation()
    {
        yield return new WaitForSeconds(1);
        if (wave == 0)
        {
            Propagation();
        }
        else
        {
            Propagande();
        }
        yield return new WaitForSeconds(0.5f);
        ok = true;
    }

    void Propagation()
    {
        GameObject temp;
        if (posY < 3)
        {
            temp = Instantiate(moreTears, transform.position + new Vector3(0, 1.1f, 0), Quaternion.identity);
            temp.GetComponent<TearGas>().Kiddo(1, posX, posY);
        }
        if (posY > 0)
        {
            temp = Instantiate(moreTears, transform.position + new Vector3(0, -1.1f, 0), Quaternion.identity);
            temp.GetComponent<TearGas>().Kiddo(3, posX, posY);
        }
        if (posX > 0)
        {
            temp = Instantiate(moreTears, transform.position + new Vector3(-2.1f, 0, 0), Quaternion.identity);
            temp.GetComponent<TearGas>().Kiddo(0, posX, posY);
        }
        if (posX < gm.maxSens0 - 1)
        {
            temp = Instantiate(moreTears, transform.position + new Vector3(2.1f, 0, 0), Quaternion.identity);
            temp.GetComponent<TearGas>().Kiddo(2, posX, posY);
        }
    }

    void Propagande()
    {
        GameObject temp;
        if (sens == 0)
        {
            if (posX > 0)
            {
                temp = Instantiate(moreTears, transform.position + new Vector3(-2.1f, 0, 0), Quaternion.identity);
                temp.GetComponent<TearGas>().Kiddo(0, posX, posY);
            }
        }
        if (sens == 1)
        {
            if (posY < 3)
            {
                temp = Instantiate(moreTears, transform.position + new Vector3(0, 1.1f, 0), Quaternion.identity);
                temp.GetComponent<TearGas>().Kiddo(1, posX, posY);
            }
        }
        if (sens == 2)
        {
            if (posX < gm.maxSens0 - 1)
            {
                temp = Instantiate(moreTears, transform.position + new Vector3(2.1f, 0, 0), Quaternion.identity);
                temp.GetComponent<TearGas>().Kiddo(2, posX, posY);
            }
        }
        if (sens == 3)
        {
            if (posY > 0)
            {
                temp = Instantiate(moreTears, transform.position + new Vector3(0, -1.1f, 0), Quaternion.identity);
                temp.GetComponent<TearGas>().Kiddo(3, posX, posY);
            }
        }


    }
    
}
