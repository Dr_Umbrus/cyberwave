﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawning : MonoBehaviour
{
    public List<int> spawns;
    public List<bool> side;
    public List<GameObject> objects;
    public List<int> typeSol;
    public GameManager gm;
    public Grid grille;
    public Player player;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        grille = gm.grille;
        player = gm.player;
        gm.spawn = this;
    }

    public void Spawner()
    {
        for(int i=0; i<objects.Count; i++)
        {
            if (spawns[i] == 16)
            {
                spawns[i] = Random.Range(0, 16);
            }
            GameObject temp;
            if (side[i])
            {
                temp=Instantiate(objects[i], new Vector3(gm.spawnPosR[spawns[i]].x+ objects[i].GetComponent<TileIdentity>().offsetx, gm.spawnPosR[spawns[i]].y+objects[i].GetComponent<TileIdentity>().offset, gm.spawnPosR[i].z-0.5f), Quaternion.identity, grille.transform);
                temp.GetComponent<TileIdentity>().posX = gm.battleSol[spawns[i] + gm.battleSol.Count / 2].posX;
                temp.GetComponent<TileIdentity>().posY = gm.battleSol[spawns[i] + gm.battleSol.Count / 2].posY;
            }
            else
            {
                temp=Instantiate(objects[i], new Vector3(gm.spawnPosL[spawns[i]].x+objects[i].GetComponent<TileIdentity>().offsetx, gm.spawnPosL[spawns[i]].y + objects[i].GetComponent<TileIdentity>().offset, gm.spawnPosL[i].z - 0.5f), Quaternion.identity, grille.transform);
                temp.GetComponent<TileIdentity>().posX = gm.battleSol[spawns[i]].posX;
                temp.GetComponent<TileIdentity>().posY= gm.battleSol[spawns[i]].posY;
            }
        }
        
        
    }

    IEnumerator Letgo()
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(1);
        gm.MyTurn();
    }
    public void Summon()
    {
        player.transform.position = gm.spawnPosL[5];
        player.transform.Translate(new Vector3(0, player.offset, -0.5f));
        player.posX = gm.battleSol[5].posX;
        player.posY = gm.battleSol[5].posY;
        StartCoroutine(Letgo());
    }

    public void SetSpawn(List<GameObject> what, List<bool> sid, List<int> where)
    {
        objects = what;
        side = sid;
        spawns = where;
    }
}
