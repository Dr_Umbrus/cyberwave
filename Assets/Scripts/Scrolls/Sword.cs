﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Projectile
{

    private void Start()
    {
        Destroy(gameObject, 0.15f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            collision.gameObject.GetComponent<Enemies>().Damaged(damage, paraTime);
        }
    }
}
