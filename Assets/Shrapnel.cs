﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrapnel : Projectile
{
    int speed;
    Rigidbody2D rb;

    public override void Started()
    {
        speed = Random.Range(20, 40);
        float potato = Random.Range(1, 101) / 100;
        GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        transform.Rotate(new Vector3(0, 0, Random.Range(-30, 31)));
        rb = GetComponent<Rigidbody2D>();
    }

    public override void Pattern()
    {
        rb.transform.Translate(new Vector3(speed, 0, 0) * Time.deltaTime, Space.Self);
    }
}
