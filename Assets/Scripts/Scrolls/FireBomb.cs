﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBomb : TileIdentity
{
    public int damage;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "enemy")
        {
            collision.GetComponent<Enemies>().Damaged(damage, 0);
        }
        
    }

}
