﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatManager : MonoBehaviour
{
    public GameObject[] enemyTypes;
    public bool[] sens;
    public int[] positions;
    public int whichBattle;

    public List<int> battle1;
    public List<int> pos1;
    public List<int> pos2;
    public List<int> pos3;
    public List<int> battle2;
    public List<int> battle3;

    private void Awake()
    {
        for(int i=0; i<positions.Length; i++)
        {
            positions[i] = i;
        }
    }
    public void SetSpawner(Spawning t, int what)
    {
        List<GameObject> enemies = new List<GameObject>();
        List<bool> sensed = new List<bool>();
        List<int> pos = new List<int>();

        if (what == 1)
        {
            for (int i = 0; i < battle1.Count; i++)
            {
                enemies.Add(enemyTypes[battle1[i]]);
                sensed.Add(sens[battle1[i]]);
                pos.Add(positions[pos1[i]]);
            }
        }
        else if (what == 2)
        {
            for (int i = 0; i < battle2.Count; i++)
            {
                enemies.Add(enemyTypes[battle2[i]]);
                sensed.Add(sens[battle2[i]]);
                pos.Add(positions[pos2[i]]);
            }
        }
        else if (what == 3)
        {
            for (int i = 0; i < battle3.Count; i++)
            {
                enemies.Add(enemyTypes[battle3[i]]);
                sensed.Add(sens[battle3[i]]);
                pos.Add(positions[pos3[i]]);
            }
            
        }

        t.SetSpawn(enemies, sensed, pos);
    }

}
