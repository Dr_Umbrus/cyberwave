﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mettaur : Enemies
{
    public GameObject projectile;

    public override void Starting2()
    {
        startTimeAttack += Random.Range(-10, 11) / 10;
        timeAttack = startTimeAttack;
    }
    public override void Pattern()
    {

        if (!stopped)
        {
            
            if (timeMove > 0)
            {
                timeMove -= Time.deltaTime;
            }
            else
            {
                if (player.posY > posY)
                {
                    canMove = gm.CheckMove(posX, posY, 1, 1, 1);
                    if (canMove)
                    {
                        timeMove = startTimeMove;
                        posY++;
                        transform.Translate(new Vector3(0, 1.1f, 0));
                    }
                }
                if (player.posY < posY)
                {
                    canMove = gm.CheckMove(posX, posY, 1, -1, 1);
                    if (canMove)
                    {
                        timeMove = startTimeMove;
                        posY--;
                        transform.Translate(new Vector3(0, -1.1f, 0));
                    }
                }
            }
        }

            if (player.posY == posY && timeAttack > 0 && !stopped)
            {
                timeAttack -= Time.deltaTime;
            }

            if (timeAttack <= 0)
            {
                timeAttack = startTimeAttack;
                anim.SetTrigger("Attacking");
               stopped = true;
            attacking = true;
            }

        if (sr.sprite == attackSprite && attacking)
        {
            GameObject temp = Instantiate(projectile, new Vector3(transform.position.x, transform.position.y-offset, transform.position.z), Quaternion.identity);
            temp.GetComponent<MettaurWave>().SetStats(posX, posY, -1, gm);
            attacking = false;
        }
        
        
    }
}
