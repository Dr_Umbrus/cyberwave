﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicChargeShot : Projectile
{
    bool dying = false;
    public float speed;
    SpriteRenderer sr;
    Color end;
    float endTime=0.1f;

    public override void Started()
    {
        sr = GetComponent<SpriteRenderer>();
        end = sr.color;
        end.a = 0;
    }
    public override void Pattern()
    {
        if (!dying)
            transform.Translate(new Vector3(speed * sens, 0, 0) * Time.deltaTime);
        else
        {
            endTime-=Time.deltaTime;
            if (endTime <= 0)
            {
                Destroy(gameObject);
            }
            sr.color = Color.Lerp(sr.color, end, endTime);
            transform.Translate(new Vector3(speed * sens, 0, 0) * Time.deltaTime/100);
            transform.localScale = new Vector3(Mathf.Lerp(0,1,endTime), transform.localScale.y, transform.localScale.z);
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!dying)
        {
            if (collision.gameObject.tag == "enemy")
            {
                collision.gameObject.GetComponent<Enemies>().Damaged(damage, paraTime);
            }

            if (collision.gameObject.tag != "player")
            {
                dying = true;
            }
        }
    }
}