﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinderBlow : Enemies
{
    public GameObject windy;
    public override void Pattern()
    {
        if (sr.sprite == attackSprite)
        {
            Destroy(Instantiate(windy, new Vector3(0, 0, -5), Quaternion.identity), 2);
            timeAttack = startTimeAttack;
            for (int i = 0; i < gm.battleEntities.Count; i++)
            {
                if (gm.battleEntities[i].side == 0)
                {
                    while (gm.battleEntities[i].canMove)
                    {
                        gm.battleEntities[i].canMove = gm.CheckMove(gm.battleEntities[i].posX, gm.battleEntities[i].posY, 0, -1, 0);
                        if (gm.battleEntities[i].canMove)
                        {
                            gm.battleEntities[i].posX--;
                            gm.battleEntities[i].transform.Translate(new Vector3(-2.15f, 0, 0));
                        }
                    }
                }
            }
        }
    }
}