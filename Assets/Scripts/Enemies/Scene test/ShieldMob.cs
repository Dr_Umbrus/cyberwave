﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldMob : Enemies
{
    public GameObject projectile;
    public float timeShield, startTimeShield;

    public override void Starting2()
    {
        startTimeAttack += Random.Range(-10, 11) / 10;
        timeAttack = startTimeAttack;
    }
    public override void Pattern()
    {

        if (sr.sprite == baseSprite && timeShield<=0)
        {
            shielded = true;
        }
        else
        {
            shielded = false;
        }
        if (timeShield > 0)
        {
            timeShield -= Time.deltaTime;
        }


        if (player.posY == posY && timeAttack > 0 && !stopped)
        {
            timeAttack -= Time.deltaTime;
        }
        if (player.posY != posY)
        {
            timeAttack = 0.1f;
        }

        if (timeAttack <= 0)
        {
            timeAttack = startTimeAttack;
            anim.SetTrigger("Attacking");
            stopped = true;
            attacking = true;
        }

        if (sr.sprite == attackSprite && attacking)
        {
            GameObject temp = Instantiate(projectile, new Vector3(transform.position.x, transform.position.y - offset, transform.position.z), Quaternion.identity);
            temp.GetComponent<MettaurWave>().SetStats(posX, posY, -1, gm);
            attacking = false;
            timeShield = startTimeShield;
        }


    }
}
