﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal50 : Scrolls
{
    GameManager gm;
    public GameObject heal;
    public int healValue;

    public override void Starting()
    {
        gm = FindObjectOfType<GameManager>();
    }

    public override void Casting(int dakka, float stun)
    {
        GameObject temp=Instantiate(heal, new Vector3(player.transform.position.x, player.transform.position.y + 0.1f, player.transform.position.z-0.5f), Quaternion.identity, player.transform);
        temp.GetComponent<HealVisual>().healValue = healValue;
    }
}
