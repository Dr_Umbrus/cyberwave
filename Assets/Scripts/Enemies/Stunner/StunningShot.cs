﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunningShot : Projectile
{
    public BoxCollider2D[] col;
    public float blinkTime, maxBlinkTime;
    public Color startCol, clignoteCol;
    public List<SpriteRenderer> targetCases;
    public GameObject[] gO;
    public Stunner father;
    public SpriteRenderer sr;
    public Sprite activate, end;
    AudioSource audioS;


    private void Start()
    {
        audioS = GetComponent<AudioSource>();
        audioS.enabled = false;
        bool verif=false;
        foreach (BoxCollider2D coli in col)
        {
                coli.enabled = false;

        }
        for (int i = 0; i < gm.battleSol.Count; i++)
        {
            if(gm.battleSol[i].posX==posX && gm.battleSol[i].posY == posY)
            {
                verif = true;
            }
            else
            {
                verif = false;
            }
            if (gm.battleSol[i].posX <posX + 2 && gm.battleSol[i].posY < posY+2 && gm.battleSol[i].posX >posX - 2 && gm.battleSol[i].posY >posY-2 && !verif)
            {
                targetCases.Add(gm.battleSol[i].GetComponent<SpriteRenderer>());
            }
        }

        foreach(GameObject game in gO)
        {
            game.SetActive(false);
        }
        

    }

    public void Father(Stunner potato)
    {
        father = potato;
        father.stopped = true;
        sr = father.GetComponent<SpriteRenderer>();
    }



    public override void Pattern()
    {
        if (blinkTime >= 0)
        {
            blinkTime -= Time.deltaTime;

        }
        else
        {
            blinkTime = maxBlinkTime;
            foreach (SpriteRenderer cases in targetCases)
            {

                if (cases.color == clignoteCol)
                {
                    cases.color = startCol;
                }
                else
                {
                    cases.color = clignoteCol;
                }
            }


        }
        if (sr != null)
        {
            if (sr.sprite == activate)
            {
                audioS.enabled = true;
                foreach (GameObject potato in gO)
                {
                    potato.SetActive(true);
                }
                foreach (BoxCollider2D coli in col)
                {
                    coli.enabled = true;

                }
            }

            if (sr.sprite == end)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
        


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            collision.gameObject.GetComponent<Player>().Damaged(paraTime, damage);
        }
    }

    private void OnDestroy()
    {
        foreach (SpriteRenderer cases in targetCases)
        {

            if (cases.color != startCol)
            {
                cases.color = startCol;
            }
        }
    }
}
