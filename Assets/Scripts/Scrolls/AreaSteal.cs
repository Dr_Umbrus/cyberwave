﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaSteal : Scrolls
{
    GameManager gm;
    public GameObject AreaProj;
    public Transform[] Xsummon;
    public GameObject marouflage;

    public override void Starting()
    {
        gm = FindObjectOfType<GameManager>();
    }

    public override void Casting(int dakka, float stun)
    {
        marouflage.SetActive(true);
        Time.timeScale = 0;
        GameObject temp=Instantiate(AreaProj, new Vector3(Xsummon[gm.maxSens0].position.x, 0,-5), Quaternion.identity);
        temp.GetComponent<AreaStealProj>().Father(this);
        
    }
    public void Stealing()
    {
        bool[] bloqued = new bool[4];
        for (int i = 0; i < 4; i++)
        {
            bloqued[i] = false;
        }
        for (int i = 0; i < gm.battleEntities.Count; i++)
        {
            if (gm.battleEntities[i].posX == gm.maxSens0)
            {
                bloqued[gm.battleEntities[i].posY] = true;
                if (gm.battleEntities[i].gameObject.tag == "enemy")
                {
                    gm.battleEntities[i].gameObject.GetComponent<Enemies>().Damaged(10, 0);
                }
            }
        }
        for (int i = 0; i < gm.battleSol.Count; i++)
        {
            if (gm.battleSol[i].posX == gm.maxSens0 && !bloqued[gm.battleSol[i].posY])
            {
                gm.battleSol[i].side = 0;
                gm.battleSol[i].GetComponentInChildren<ColorSol>().Coloring();
            }
        }

        bool test = false;
        for (int i = 0; i < 4; i++)
        {
            if (bloqued[i])
            {
                test = true;
            }
        }

        if (!test && gm.maxSens0 < 5)
        {
            gm.maxSens0++;
        }
        Time.timeScale = 1;
        marouflage.SetActive(false);
    }
}
