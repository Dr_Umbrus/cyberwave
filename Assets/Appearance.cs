﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Appearance : MonoBehaviour
{
    public float timeToChange;
    float timer=0;
    Color a = new Color(1, 1, 1, 0), b = new Color(1, 1, 1, 1);
    SpriteRenderer sr;
    bool disappearing = false;

    private void Awake()
    {
        if (sr == null)
        {
            sr = GetComponent<SpriteRenderer>();
            sr.color = a;
            timer = 0;
        }
    }
    void Update()
    {
        if (timer < timeToChange)
        {
            timer += Time.deltaTime;
            sr.color = Color.Lerp(a, b, timer / timeToChange);
        }
        if (disappearing)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
                sr.color = Color.Lerp(a, b, timer / timeToChange);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    public void Death()
    {
        disappearing = true;
    }
}
