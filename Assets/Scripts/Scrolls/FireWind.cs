﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWind : Projectile
{
    public int speed;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            collision.gameObject.GetComponent<Enemies>().Damaged(damage, paraTime);
            Destroy(gameObject);
        }
    }

    public override void Pattern()
    {
        transform.Translate(new Vector3(speed * sens, 0, 0) * Time.deltaTime);
    }
}
