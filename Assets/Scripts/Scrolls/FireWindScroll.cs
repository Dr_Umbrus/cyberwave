﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWindScroll : Scrolls
{
    public GameObject fire;
    public Vector3[] launchPos;
    public Player playerScript=null;


    public override void Starting()
    {
            playerScript = FindObjectOfType<Player>();
    }
    public override void Casting(int dakka, float stun)
    {

        StartCoroutine(FireShoot(dakka, stun));
    }

    IEnumerator FireShoot(int dakka, float stun)
    {

        GameObject temp;
        temp=Instantiate(fire, launchPos[playerScript.posY], Quaternion.identity);
        temp.GetComponent<Projectile>().MoreDakka(dakka, stun);
        yield return new WaitForSeconds(1);
        temp = Instantiate(fire, launchPos[playerScript.posY], Quaternion.identity);
        temp.GetComponent<Projectile>().MoreDakka(dakka, stun);
        yield return new WaitForSeconds(1);
        temp = Instantiate(fire, launchPos[playerScript.posY], Quaternion.identity);
        temp.GetComponent<Projectile>().MoreDakka(dakka, stun);
    }
}
