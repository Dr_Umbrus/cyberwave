﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBoum : TileIdentity
{
    public int damage;
    public float paraTime;
    List<GameObject> hit= new List<GameObject>();
    SpriteRenderer sr;
    public Sprite sprite;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (sr.sprite==sprite)
        {
            Destroy(gameObject);
        }
    }

    public void MoreDakka(int dakka, float dur)
    {
        damage += dakka;
        paraTime += dur;

        for (int i = 0; i < gm.battleEntities.Count; i++)
        {
            if (gm.battleEntities[i].posX == posX  && gm.battleEntities[i].posY == posY)
            {
                if (gm.battleEntities[i].GetComponent<Enemies>() != null)
                    gm.battleEntities[i].GetComponent<Enemies>().Damaged(damage, paraTime);
                hit.Add(gm.battleEntities[i].gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "enemy")
        {
            bool touch = true;
            if (hit.Count > 0)
            {
                for (int i = 0; i < hit.Count; i++)
                {
                    if (hit[i] == collision.gameObject)
                    {
                        touch=false;
                    }
                }
            }
            if (touch == true)
            {
                collision.GetComponent<Enemies>().Damaged(damage, 0);
                hit.Add(collision.gameObject);
            }
            
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < gm.battleEntities.Count; i++)
        {
            if(gm.battleEntities[i].posX>posX-2 && gm.battleEntities[i].posX < posX +2 && gm.battleEntities[i].posY > posY-2 && gm.battleEntities[i].posY < posY + 2)
            {
                if(gm.battleEntities[i].GetComponent<Enemies>()!=null)
                gm.battleEntities[i].GetComponent<Enemies>().Damaged(damage, paraTime);
            }
        }
    }
}
